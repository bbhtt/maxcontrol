/**
 * Max Control
 * Copyright (c) 2015 Jan-Michael Brummer
 *
 * This file is part of Max Control.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 only.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <stdlib.h>

#include <glib.h>
#include <gio/gio.h>

#include <libsoup/soup.h>

#include <cube.h>
#include <cube_object.h>

/*#define DEBUG_CUBE_DEVICE_LIST 1 */
/*#define DEBUG_CUBE_HEADER 1 */
/*#define DEBUG_CUBE_METADATA 1 */
/*#define DEBUG_CUBE_CONFIG_CUBE 1 */
/*#define DEBUG_CUBE_CONFIG_DEVICE 1 */
/*#define DEBUG_CUBE_CONFIG 1 */
/*#define DEBUG_CUBE_TRAFFIC 1 */

/** Cancel timeout for network transfer in seconds */
static int cancel_timeout = 5;

/** Currently active cube */
static struct cube *active_cube = NULL;

static gboolean cube_parse_encryption (struct cube *cube,
                                       gpointer     incoming_data);
static gboolean cube_parse_decryption (struct cube *cube,
                                       gpointer     incoming_data);

/**
 * \brief Convert input date to yy-mm-dd
 * \param val input date
 * \return formatted date
 */
gchar *cube_convert_date(gchar *val)
{
  gint year;
  gint month;
  gint day;
  gchar hex[3];

  hex[0] = val[0];
  hex[1] = val[1];
  hex[2] = '\0';
  year = 2000 + strtol (hex, NULL, 16);

  hex[0] = val[2];
  hex[1] = val[3];
  hex[2] = '\0';
  month = strtol (hex, NULL, 16);

  hex[0] = val[4];
  hex[1] = val[5];
  hex[2] = '\0';
  day = strtol (hex, NULL, 16);

  return g_strdup_printf ("%d-%d-%d", year, month, day);
}

/**
 * \brief Convert input time to hh:mm
 * \param val input time
 * \return formatted time
 */
gchar *cube_convert_time(gchar *val)
{
  gint hour;
  gint min;
  gchar hex[3];

  hex[0] = val[0];
  hex[1] = val[1];
  hex[2] = '\0';
  hour = strtol (hex, NULL, 16);

  hex[0] = val[2];
  hex[1] = val[3];
  hex[2] = '\0';
  min = strtol (hex, NULL, 16);

  return g_strdup_printf ("%d:%d", hour, min);
}

/**
 * \brief Compute until date
 * \param day day
 * \param month month
 * \param year year
 * \return computed until time number
 */
gint cube_compute_date(gint day,
                       gint month,
                       gint year)
{
  gint date;

  date = (month & 0xE) << 12;
  date += (day << 8);
  date += (month & 0x1) << 6;
  date += (year - 2000);
  g_print ("date: %x\n", date);

  return date;
}

/**
 * \brief Compute until time
 * \param hour hour
 * \param min minute
 * \return computed until time number
 */
gint cube_compute_time(gint hour,
                       gint min)
{
  gint time;

  time = hour * 2 + min;

  return time;
}

/**
 * \brief Get date information out of date number
 * \param until date incoming until date
 * \param year out year
 * \param month out month
 * \param day out day
 */
void cube_get_date(gint  until_date,
                   gint *day,
                   gint *month,
                   gint *year)
{
  *day = (until_date >> 8) & 0x1F;
  *month = ((until_date & 0xE000) >> 12) + ((until_date >> 6) & 0x1);
  *year = (until_date & 0x1F) + 2000;
}

/**
 * \brief Get time information out of time number
 * \param until time incoming until time
 * \param hour out hour
 * \param min out min
 */
void cube_get_time(gint  until_time,
                   gint *hour,
                   gint *min)
{
  *hour = (until_time / 2) / 2;
  *min = (until_time / 2) % 2;
}

/**
 * \brief Wrapper class for base64 allowing us to print the hex data before converting
 * \param msg input message
 * \param len length of input message
 * \return base64 encoded message string (see g_base64_encode())
 */
static gchar *cube_encode(const guchar *msg,
                          gsize         len)
{
#ifdef DEBUG_CUBE_TRAFFIC
  gint i;

  g_print (" ** Data: ");
  for (i = 0; i < len; i++) {
    g_print ("%2.2x ", msg[i]);
  }

  g_print ("\n");
#endif

  return g_base64_encode (msg, len);
}

/**
 * \brief Wrapper class for base64 allowing us to print the hex data after converting
 * \param msg input message
 * \return base64 decoded message string (see g_base64_decode())
 */
static guchar *cube_decode(const gchar *in,
                           gsize       *len)
{
  guchar *msg;

  msg = g_base64_decode (in, len);

#ifdef DEBUG_CUBE_TRAFFIC
  gint i;

  g_print (" ** Data: ");
  for (i = 0; i < *len; i++) {
    g_print ("%2.2x ", msg[i]);
  }

  g_print ("\n");
#endif

  return msg;
}

/**
 * \brief Cancel threads - sleeps cancel_timeout seconds and then toggles gcancellable
 * \param data cancellable object
 * \return NULL
 */
static gpointer cancel_thread(gpointer data)
{
  GCancellable *cancellable = data;

  g_usleep (1000 * 1000 * cancel_timeout);
  g_cancellable_cancel (cancellable);

  return NULL;
}

/**
 * \brief Detects all cube in the given network range
 * \return list of detected cubes
 */
GList *cube_detect(gchar *ip)
{
  GSocket *socket;
  GSocketAddress *own_address;
  GSocketAddress *mc_address;
  GSocketAddress *cli_address;
  GInetAddress *inet_address;
  GError *error = NULL;
  gchar recvline[32];
  GCancellable *cancellable;
  gssize len;
  GList *list = NULL;
  gchar cube_detect_request[19] = {
    0x65, 0x51, 0x33, 0x4d,
    0x61, 0x78, 0x2a, 0x00,
    0x2a, 0x2a, 0x2a, 0x2a,
    0x2a, 0x2a, 0x2a, 0x2a,
    0x2a, 0x2a, 0x49
  };

  /* Create IPV4 UDP Socket */
  socket = g_socket_new (G_SOCKET_FAMILY_IPV4, G_SOCKET_TYPE_DATAGRAM, G_SOCKET_PROTOCOL_DEFAULT, &error);

  /* Create address for IPV4 UDP port */
  own_address = g_inet_socket_address_new (g_inet_address_new_any (G_SOCKET_FAMILY_IPV4), MAX_UDP_PORT);
  if (!g_socket_bind (socket, own_address, TRUE, &error)) {
    g_error ("Error on bind: %s\n", error ? error->message : "");

    g_object_unref (socket);

    return NULL;
  }

  /* Create multicast address */
  inet_address = g_inet_address_new_from_string (ip ? ip : MULTICAST);
  if (!inet_address) {
    g_object_unref (own_address);
    g_object_unref (socket);
    return NULL;
  }

  mc_address = g_inet_socket_address_new (inet_address, MAX_UDP_PORT);
  if (!mc_address) {
    g_object_unref (inet_address);
    g_object_unref (own_address);
    g_object_unref (socket);
    return NULL;
  }

  if (g_socket_send_to (socket, mc_address, cube_detect_request, sizeof (cube_detect_request), NULL, &error) == -1) {
    g_error ("Error on send: %s\n", error ? error->message : "");

    g_object_unref (mc_address);
    g_object_unref (own_address);
    g_object_unref (socket);

    return NULL;
  }

  /* Create cancel thread to stop scanning for max cubes */
  cancellable = g_cancellable_new ();
  g_thread_new ("cancel thread", cancel_thread, cancellable);

  /* Try to find a max cube until cancellable jumps in */
  do {
    /* Read multicast line */
    memset (recvline, 0, sizeof (recvline));
    len = g_socket_receive_from (socket, &cli_address, recvline, sizeof (recvline), cancellable, &error);

    /* Cube broadcast response is always of length 26 and starts with eQ3MaxAp */
    if (len == 26 && !strncmp (recvline, "eQ3MaxAp", 8)) {
      struct cube *cube = g_slice_alloc0 (sizeof (struct cube));

      cube->address = g_inet_socket_address_get_address (G_INET_SOCKET_ADDRESS (cli_address));

      cube->detect_header.response = g_string_new_len (recvline + 0, 8);
      cube->detect_header.serial = g_string_new_len (recvline + 8, 10);
      cube->detect_header.reserved = g_string_new_len (recvline + 17, 4);
      cube->detect_header.rf_address = ((recvline[21] & 0xFF) << 16) + ((recvline[22] & 0xFF) << 8) + (recvline[23] & 0xFF);
      cube->detect_header.fw_version = (recvline[24] << 8) + (recvline[25] & 0xFF);

      list = g_list_append (list, cube);

      break;
    }
  } while (!g_cancellable_is_cancelled (cancellable));

  /* Close socket */
  if (!g_socket_close (socket, &error)) {
    g_error ("Cannot close socket: %s\n", error ? error->message : "");

    /* Just fall-through */
  }

  g_object_unref (inet_address);
  g_object_unref (mc_address);
  g_object_unref (own_address);
  g_object_unref (socket);

  return list;
}

/**
 * \brief Parse header (H:) message of cube
 * \param cube cube pointer
 * \param incoming_data incoming data
 * \return TRUE if metadata has been parsed successfully, otherwise FALSE
 */
static gboolean cube_parse_header(struct cube *cube,
                                  gpointer     incoming_data)
{
  gchar **split = g_strsplit (incoming_data, ",", -1);

  if (g_strv_length (split) != 11) {
    g_warning ("%s(): header does not fit %d\n", __FUNCTION__, g_strv_length (split));
    g_strfreev (split);
    return FALSE;
  }

  cube->header.serial_number = g_strdup (split[0]);
  cube->header.rf_address = g_strdup (split[1]);
  cube->header.fw_version = g_strdup (split[2]);
  cube->header.unknown = g_strdup (split[3]);
  cube->header.http_connect_id = g_strdup (split[4]);
  cube->header.duty_cycle = g_strdup (split[5]);
  cube->header.free_memory_slots = g_strdup (split[6]);
  cube->header.cube_date = g_strdup (split[7]);
  cube->header.cube_time = g_strdup (split[8]);
  cube->header.state_cube_time = g_strdup (split[9]);
  cube->header.ntp_counter = g_strdup (split[10]);

#ifdef DEBUG_CUBE_HEADER
  g_print ("Serial number     : %s\n", cube->header.serial_number);
  g_print ("RF address        : %s\n", cube->header.rf_address);
  g_print ("Firmware version  : %s\n", cube->header.fw_version);
  g_print ("Unknown           : %s\n", cube->header.unknown);
  g_print ("HTTP connection id: %s\n", cube->header.http_connect_id);
  g_print ("Duty cycle        : %s\n", cube->header.duty_cycle);
  g_print ("Free memory slots : %s\n", cube->header.free_memory_slots);

  gchar *date = cube_convert_date (cube->header.cube_date);
  g_print ("Cube date         : %s\n", date);
  g_free (date);
  gchar *time = cube_convert_time (cube->header.cube_time);
  g_print ("Cube time         : %s\n", time);
  g_free (time);
  g_print ("State cube time   : %s\n", cube->header.state_cube_time);
  g_print ("NTP counter       : %s\n", cube->header.ntp_counter);
#endif

  g_strfreev (split);

  return TRUE;
}

/**
 * \brief Returns the device string by given device type
 * \param type device type
 * \return device name
 */
gchar *cube_device_string(eCubeDeviceType type)
{
  switch (type) {
    case CUBE_DEVICE_TYPE_CUBE:
      return _("Cube");
    case CUBE_DEVICE_TYPE_HEATING:
      return _("Heating Thermostat");
    case CUBE_DEVICE_TYPE_HEATING_PLUS:
      return _("Heating Thermostat Plus");
    case CUBE_DEVICE_TYPE_WALL:
      return _("Wall Thermostat");
    case CUBE_DEVICE_TYPE_SHUTTER:
      return _("Shutter contact");
    case CUBE_DEVICE_TYPE_BUTTON:
      return _("Push Button");
    default:
      return "";
  }

  return "";
}

/**
 * \brief Parse metadata (M:)
 * \param cube cube pointer
 * \param incoming_data incoming data
 * \return TRUE if metadata has been parsed successfully, otherwise FALSE
 */
static gboolean cube_parse_metadata(struct cube *cube,
                                    gpointer     incoming_data)
{
  gsize data_len;
  guchar *data;
  gint room_count;
  gint device_count;
  gint pos;
  gint j;
  gchar **split = g_strsplit (incoming_data, ",", -1);

  if (g_strv_length (split) != 3) {
    g_warning ("%s(): header does not fit %d\n", __FUNCTION__, g_strv_length (split));
    g_strfreev (split);
    return FALSE;
  }

#ifdef DEBUG_CUBE_METADATA
  /* Index - unhandled */
  g_print ("Index: %s\n", split[0]);
  /* Count - unhandled */
  g_print ("Count: %s\n", split[1]);
#endif

  /* Decode base64 data */
  data = g_base64_decode (split[2], &data_len);

  /* Free split */
  g_strfreev (split);

  /* Check for valid header */
  if (data[0] != 0x56 || data[1] != 0x02) {
    if (data[0] == 0xEF && data[1] == 0x7D) {
      g_warning ("Initial state detected, please configure your cube first\n");
    } else {
      g_warning ("error parsing data (%x/%x)\n", data[0], data[1]);
    }

    g_free (data);
    return FALSE;
  }

  /* Skip first two bytes, checked above */
  pos = 2;

  /* Read number of rooms */
  room_count = data[pos++];

  /* Rooms */
  for (j = 0; j < room_count; j++) {
    struct cube_room *room = g_slice_alloc0 (sizeof (struct cube_room));

    /* ID */
    room->id = data[pos++];

    /* Name */
    if (data[pos]) {
      room->name = g_string_new_len ((gchar *)(data + pos + 1), data[pos]);
    } else {
      room->name = g_string_new ("");
    }
    pos += 1 + room->name->len;

    /* RF address */
    room->rf_address = (data[pos + 0] << 16) + (data[pos + 1] << 8) + data[pos + 2];
    pos += 3;

    room->cube = cube;

#ifdef DEBUG_CUBE_METADATA
    g_print ("Room (%d): '%s', Address %6.6x\n", room->id, room->name->str, room->rf_address);
#endif

    /* Add room to cube's room list */
    cube->rooms = g_list_append (cube->rooms, room);
  }

  /* Read number of devices */
  device_count = data[pos++];

  /* Traverse devices */
  for (j = 0; j < device_count; j++) {
    struct cube_device *device = g_slice_alloc0 (sizeof (struct cube_device));
    struct cube_room *room;

    /* Type */
    device->type = data[pos++];

    /* RF address */
    device->rf_address = (data[pos + 0] << 16) + (data[pos + 1] << 8) + data[pos + 2];
    pos += 3;

    /* Serial number */
    device->serial_number = g_string_new_len ((gchar *)(data + pos), 10);
    pos += 10;

    /* Name */
    device->name = g_string_new_len ((gchar *)(data + pos + 1), data[pos]);
    pos += 1 + device->name->len;

    /* Room */
    device->room_id = data[pos++];

#ifdef DEBUG_CUBE_METADATA
    g_print ("Device (%s): '%s', Room %d, Address %6.6x\n", cube_device_string (device->type), device->name->str, device->room_id, device->rf_address);
#endif
    /* Cube rf address */
    device->cube_rf_address = cube->detect_header.rf_address;

    /* Add device to cube's device list */
    cube->devices = g_list_append (cube->devices, device);

    /* Add it also to the connected room */
    room = cube_get_room_by_id (cube, device->room_id);
    if (room) {
      room->devices = g_list_append (room->devices, device);
    }
  }

  g_free (data);

  /* Inform every listener about new meta data */
  g_signal_emit (cube_object, cube_object_signals[CCB_MODIFIED], 0, cube);

  return TRUE;
}

/**
 * \brief Parse cube configuration data
 * \param cube cube pointer
 * \param incoming_data incoming data
 * \return TRUE if metadata has been parsed successfully, otherwise FALSE
 */
static gboolean cube_parse_config_cube(struct cube *cube,
                                       gpointer     incoming_data)
{
  guchar *data = incoming_data;
  gint pos = 0;

#ifdef DEBUG_CUBE_CONFIG_CUBE
  g_print ("Configuration for cube\n");
#endif

  cube->config = g_slice_alloc0 (sizeof (struct cube_config));

  pos = 18;
  cube->config->portal_enabled = data[pos++];
#ifdef DEBUG_CUBE_CONFIG_CUBE
  gint i;

  g_print (" Portal enabled: %d\n", cube->config->portal_enabled);

  g_print (" Portal url: ");
  pos = 85;
  for (i = 0; i < 36; i++) {
    g_print ("%c", data[pos++] & 0xFF);
  }
  g_print ("\n");
#endif

  return TRUE;
}

/**
 * \brief Parse heating configuration data
 * \param cube cube pointer
 * \param incoming_data incoming data
 * \return TRUE if metadata has been parsed successfully, otherwise FALSE
 */
static gboolean cube_parse_config_heating(struct cube_device_config *config,
                                          gpointer                   incoming_data)
{
  guchar *data = incoming_data;
  gint pos = 5;
  gint i;

  /* Next 3 bytes are unknown (most likely 2 bytes fw, 1byte test result) */
  config->unknown = (data[pos + 0] << 16) + (data[pos + 1] << 8) + data[pos + 2];
  pos += 3;

  config->serial_number = g_string_new_len ((gchar *)(data + pos), 10);
  pos += 10;

  config->comfort_temp = (float)(data[pos++]) / 2.0f;
  config->eco_temp = (float)(data[pos++]) / 2.0f;
  config->max_set_point_temp = (float)(data[pos++]) / 2.0f;
  config->min_set_point_temp = (float)(data[pos++]) / 2.0f;

  /* temp offset * 2, offset is between -3.5 and +3.5 (default) degress => 0..7 */
  config->temp_offset = (float)(data[pos++]) / 2.0f - 3.5f;
  config->window_open_temp = (float)(data[pos++]) / 2.0f;
  config->window_open_duration = data[pos++];

  /**
   * Boost Duration and Boost Valve Value
   * The 3 MSB bits gives the duration, the 5 LSB bits the Valve Value%.
   * Duration: With 3 bits, the possible values (Dec) are 0 to 7, 0 is not used.
   * The duration in Minutes is: if Dec value = 7, then 30 minutes, else Dec value * 5 minutes
   * Valve Value: dec value 5 LSB bits * 5 gives Valve Value in %
   */
  config->boost_duration_val = data[pos++];

  /**
   * Decalcification: Day of week and Time
   * In bits: DDDHHHHH
   * The three most significant bits (MSB) are presenting the day, Saturday = 0, Friday = 6
   * The five least significant bits (LSB) are presenting the time (in hours)
   */
  config->decalcification = data[pos++];

  config->max_val_setting = data[pos++];
  config->val_offset = data[pos++];

#ifdef DEBUG_CUBE_CONFIG_DEVICE
  gint duration;
  gint value;
  gint day;
  gint hour;

  g_print (" unknown             : %d\n", config->unknown);
  g_print (" serial number       : %s\n", config->serial_number->str);
  g_print (" comfort temp        : %.1f\n", config->comfort_temp);
  g_print (" eco temp            : %.1f\n", config->eco_temp);
  g_print (" max set point temp  : %.1f\n", config->max_set_point_temp);
  g_print (" min set point temp  : %.1f\n", config->min_set_point_temp);
  g_print (" temp offset         : %.1f\n", config->temp_offset);
  g_print (" window open temp    : %.1f\n", config->window_open_temp);
  g_print (" window open duration: %d\n", config->window_open_duration);

  duration = config->boost_duration_val >> 5;
  value = config->boost_duration_val & 0x1F;
  g_print (" boost duration (%d) : duration = %dmin, val = %d%%\n", config->boost_duration_val, duration == 7 ? 30 : duration * 5, value * 5);

  day = config->decalcification >> 5;
  hour = config->decalcification & 0x1F;
  g_print (" decalcification (%d): day = %d, hour = %d\n", config->decalcification, day, hour);
  g_print (" max valve setting   : %d%%\n", config->max_val_setting * 100 / 255);
  g_print (" valve offset        : %d%%\n", config->val_offset * 100 / 255);
#endif

  /* Weekly program (Sat, Sun, Mon, Tue, Weg, Thu, Fri) */
  for (i = 0; i < 7; i++) {
    GList *list = NULL;
    gint j;

    for (j = 0; j < 13; j++) {
      struct cube_day_entry *entry = g_slice_alloc0 (sizeof (struct cube_day_entry));
      gint val;

      /* Extract degree and number of minutes of given period */
      val = (data[pos + 0] << 8) + data[pos + 1];
      entry->deg = (float)(val >> 9) / 2.0f;
      entry->min = val & 0x1FF;
      pos += 2;

      list = g_list_append (list, entry);
    }

    config->days[i] = list;

#ifdef DEBUG_CUBE_CONFIG_DEVICE
    g_print (" Day %d               : ", i);
    for (list = config->days[i]; list != NULL; list = list->next) {
      struct cube_day_entry *entry = list->data;

      g_print ("%.1fC -> %2.2d:%2.2d", entry->deg, entry->min * 5 / 60, entry->min * 5 % 60);

      /* We'll skip the output as we have reached the end of the day */
      if ((entry->min * 5 / 60) == 24) {
        break;
      }

      if (list->next) {
        g_print (", ");
      }
    }
    g_print ("\n");
#endif
  }

  return TRUE;
}

/**
 * \brief Parse wall thermostat configuration data
 * \param cube cube pointer
 * \param incoming_data incoming data
 * \return TRUE if metadata has been parsed successfully, otherwise FALSE
 */
static gboolean cube_parse_config_wall_thermostat(struct cube_device_config *config,
                                                  gpointer                   incoming_data)
{
  guchar *data = incoming_data;
  gint pos = 5;
  gint i;

  /* Next 3 bytes are unknown (most likely 2bytes fw, 1byte test result) */
  config->unknown = (data[pos + 0] << 16) + (data[pos + 1] << 8) + data[pos + 2];
  pos += 3;

  config->serial_number = g_string_new_len ((gchar *)(data + pos), 10);
  pos += 10;

  g_print ("d: %x\n", data[pos]);
  config->comfort_temp = (float)(data[pos++]) / 2.0f;
  g_print ("d: %x\n", data[pos]);
  config->eco_temp = (float)(data[pos++]) / 2.0f;
  g_print ("d: %x\n", data[pos]);
  config->max_set_point_temp = (float)(data[pos++]) / 2.0f;
  g_print ("d: %x\n", data[pos]);
  config->min_set_point_temp = (float)(data[pos++]) / 2.0f;

#ifdef DEBUG_CUBE_CONFIG_DEVICE
  g_print (" unknown           : %d\n", config->unknown);
  g_print (" serial number     : %s\n", config->serial_number->str);
  g_print (" comfort temp      : %.1f\n", config->comfort_temp);
  g_print (" eco temp          : %.1f\n", config->eco_temp);
  g_print (" max set point temp: %.1f\n", config->max_set_point_temp);
  g_print (" min set point temp: %.1f\n", config->min_set_point_temp);
#endif

  /* Weekly program (Sat, Sun, Mon, Tue, Weg, Thu, Fri) */
  for (i = 0; i < 7; i++) {
    GList *list = NULL;
    gint j;

    for (j = 0; j < 13; j++) {
      struct cube_day_entry *entry = g_slice_alloc0 (sizeof (struct cube_day_entry));
      gint val;

      /* Extract degree and number of minutes of given period */
      val = (data[pos + 0] << 8) + data[pos + 1];
      entry->deg = (float)(val >> 9) / 2.0f;
      entry->min = val & 0x1FF;
      pos += 2;
      list = g_list_append (list, entry);
    }

    config->days[i] = list;

#ifdef DEBUG_CUBE_CONFIG_DEVICE
    g_print (" Day %d             : ", i);
    for (list = config->days[i]; list != NULL; list = list->next) {
      struct cube_day_entry *entry = list->data;

      g_print ("%.1fC -> %2.2d:%2.2d", entry->deg, entry->min * 5 / 60, entry->min * 5 % 60);

      /* We'll skip the output as we have reached the end of the day */
      if ((entry->min * 5 / 60) == 24) {
        break;
      }
      if (list->next) {
        g_print (", ");
      }
    }
    g_print ("\n");
#endif
  }

  return TRUE;
}

/**
 * \brief Parse device configuration data
 * \param cube cube pointer
 * \param incoming_data incoming data
 * \return TRUE if metadata has been parsed successfully, otherwise FALSE
 */
static gboolean cube_parse_config_device(struct cube_device *device,
                                         gpointer            incoming_data)
{
  struct cube_device_config *config = g_slice_alloc0 (sizeof (struct cube_device_config));
  guchar *data = incoming_data;
  gint pos = 0;
  gboolean ret = FALSE;

#ifdef DEBUG_CUBE_CONFIG_DEVICE
  g_print ("Configuration for device\n");
#endif

  device->config = config;

  pos++;
  config->rf_address = (data[pos + 0] << 16) + (data[pos + 1] << 8) + data[pos + 2];
  pos += 3;

  config->type = data[pos++];

  switch (config->type) {
    case CUBE_DEVICE_TYPE_HEATING:
      ret = cube_parse_config_heating (config, data);
      break;
    case CUBE_DEVICE_TYPE_WALL:
      ret = cube_parse_config_wall_thermostat (config, data);
      break;
    default:
      /* Next 3 bytes are unknown */
      config->unknown = (data[pos + 0] << 16) + (data[pos + 1] << 8) + data[pos + 2];
      pos += 3;

      config->serial_number = g_string_new_len ((gchar *)(data + pos), 10);
      pos += 10;

#ifdef DEBUG_CUBE_CONFIG_DEVICE
      /*g_print(" len          : " G_GSIZE_FORMAT "\n", strlen((gchar*)data)); */
      g_print (" unknown      : %d\n", config->unknown);
      g_print (" serial number: %s\n", config->serial_number->str);
#endif
      break;
  }

  return ret;
}

/**
 * \brief Parse configuration data
 * \param cube cube pointer
 * \param incoming_data incoming data
 * \return TRUE if metadata has been parsed successfully, otherwise FALSE
 */
static gboolean cube_parse_config(struct cube *cube,
                                  gpointer     incoming_data)
{
  struct cube_device *device = NULL;
  gchar **split = g_strsplit (incoming_data, ",", -1);
  gint rf_address;
  guchar *data;
  gsize data_len;

  if (g_strv_length (split) != 2) {
    g_warning ("Something wrong here, abort\n");
    g_strfreev (split);
    return FALSE;
  }

  rf_address = strtol (split[0], NULL, 16);
#ifdef DEBUG_CUBE_CONFIG
  g_print ("Address: %6.6x\n", rf_address);
#endif

  data = g_base64_decode (split[1], &data_len);
  g_strfreev (split);

  if (data_len - 1 != data[0]) {
    g_free (data);
    return FALSE;
  }

  if (cube->detect_header.rf_address == rf_address) {
    cube_parse_config_cube (cube, data);
  } else {
    device = cube_get_device_by_address (cube, rf_address);

#if 0
    gboolean bounded = TRUE;

    if (!device) {
      /* Device is configured, but not bounded to a room */
#ifdef DEBUG_CUBE_CONFIG
      g_print ("Device not found\n");
#endif
      device = g_slice_alloc0 (sizeof (struct cube_device));
      device->room_id = -1;
      device->cube_rf_address = cube->detect_header.rf_address;

      cube->devices = g_list_append (cube->devices, device);
      bounded = FALSE;
    }

    cube_parse_config_device (device, data);

    if (!bounded) {
      device->type = device->config->type;
      device->rf_address = device->config->rf_address;
      device->serial_number = g_string_new (device->config->serial_number->str);
    }
#else
    if (device) {
      cube_parse_config_device (device, data);
    }
#endif
  }
  g_free (data);

  return TRUE;
}

/**
 * \brief Parse device list
 * \param cube cube pointer
 * \param incoming_data incoming data
 * \return TRUE if metadata has been parsed successfully, otherwise FALSE
 */
static gboolean cube_parse_device_list(struct cube *cube,
                                       gpointer     incoming_data)
{
  guchar *data;
  gint pos = 0;
  gsize data_len;

  data = g_base64_decode (incoming_data, &data_len);

  while (pos < data_len) {
    struct cube_device *device;
    gint len;
    gint rf_address;

    len = data[pos++];
#ifdef DEBUG_CUBE_DEVICE_LIST
    g_print ("len          : %d\n", len);

    gint i;
    for (i = 0; i < len; i++) {
      g_print ("%2.2x ", data[pos + i]);
    }
#endif

    rf_address = (data[pos + 0] << 16) + (data[pos + 1] << 8) + data[pos + 2];

#ifdef DEBUG_CUBE_DEVICE_LIST
    g_print ("rf_address   : %6.6x\n", rf_address);
#endif

    device = cube_get_device_by_address (cube, rf_address);
    if (!device) {
      pos += len;
      continue;
    }

    /* Skip rf address */
    pos += 3;

#ifdef DEBUG_CUBE_DEVICE_LIST
    g_print ("unknown pos: %d\n", data[pos]);
#endif
    pos++;

    device->status1 = data[pos++];
    device->status2 = data[pos++];
#ifdef DEBUG_CUBE_DEVICE_LIST
    g_print (" Type        : %d (%s)\n", device->type, cube_device_string (device->type));
    g_print (" Valid       : %d\n", device->status1 & 0x10);
    g_print (" Error       : %d\n", device->status1 & 0x8);
    g_print (" Answer      : %d\n", device->status1 & 0x4);
    g_print (" Initialized : %d\n", device->status1 & 0x2);
    g_print (" Battery low : %d\n", device->status2 & 0x80);
    g_print (" Link status : %d\n", device->status2 & 0x40);
    g_print (" Panel       : %d\n", device->status2 & 0x20);
    g_print (" Gateway     : %d\n", device->status2 & 0x10);
    g_print (" DST setting : %d\n", device->status2 & 0x8);
    g_print (" Not used    : %d\n", device->status2 & 0x4);
    g_print (" Mode        : %d\n", device->status2 & 0x3);
#endif

    if (len == 11 || len == 12) {
      gint day;
      gint month;
      gint year;
      gint hour;
      gint min;

      /*if (len == 12) {
       *       g_print("%2.2x %2.2x %2.2x %2.2x %2.2x %2.2x",
       *               data[pos + 0],
       *               data[pos + 1],
       *               data[pos + 2],
       *               data[pos + 3],
       *               data[pos + 4],
       *               data[pos + 5]);
       *  }*/
      device->valve = data[pos++];
      device->set_temp = (float)(data[pos++] & 0x3F) / 2.0f;
      device->date_until = (data[pos + 0] << 8) + data[pos + 1];
      cube_get_date (device->date_until, &day, &month, &year);
      pos += 2;
      device->time_until = data[pos++];
      cube_get_time (device->time_until, &hour, &min);

#ifdef DEBUG_CUBE_DEVICE_LIST
      if (cube_device_get_mode (device) != CUBE_MODE_AUTO) {
        g_print ("day: %d month: %d year: %d\n", day, month, year);
        g_print ("hour: %d min: %d\n", hour, min);
      }
      g_print (" Valve       : %d\n", device->valve);
      g_print (" Set temp    : %.1f\n", device->set_temp);
      /*gchar *tmp = cube_convert_date(device->date_until->str); */
      /*g_print("Date until: %s\n", tmp); */
      /*g_free(tmp); */
      /*tmp = cube_convert_time(device->time_until); */
      /*g_print("Time until: %s\n", tmp); */
      /*g_free(tmp); */
#endif
    }

    if (len == 12) {
      device->current_temp = ((data[8] & 0x80) << 1) + data[pos++];
      device->current_temp /= 10.0f;
#ifdef DEBUG_CUBE_DEVICE_LIST
      g_print (" Current temp: %.1f\n", device->current_temp);
#endif
    }
  }

  g_free (data);

  g_signal_emit (cube_object, cube_object_signals[CCB_STATUS], 0, cube);

  return TRUE;
}

/**
 * \brief Parse new device
 * \param cube cube pointer
 * \param incoming_data incoming data
 * \return TRUE if metadata has been parsed successfully, otherwise FALSE
 */
static gboolean cube_parse_new_device(struct cube *cube,
                                      gpointer     incoming_data)
{
  struct cube_device *device;
  guchar *data;
  gsize data_len;

  if (!strlen (incoming_data)) {
    return FALSE;
  }

  data = g_base64_decode (incoming_data, &data_len);

  device = g_slice_alloc0 (sizeof (struct cube_device));
  device->type = data[0];
  device->rf_address = (data[1] << 16) + (data[2] << 8) + data[3];
  device->serial_number = g_string_new_len ((gchar *)(data + 4), 10);
  device->cube_rf_address = cube->detect_header.rf_address;

  g_free (data);

  cube->devices = g_list_append (cube->devices, device);

  g_print ("New device detected. Serial=%s, Address=%6.6x, Type=%d (%s)\n",
           device->serial_number->str,
           device->rf_address,
           device->type,
           cube_device_string (device->type));

  g_signal_emit (cube_object, cube_object_signals[CCB_NEW_DEVICE], 0, device);

  return TRUE;
}

/**
 * \brief Parse send device
 * \param cube cube pointer
 * \param incoming_data incoming data
 * \return TRUE if metadata has been parsed successfully, otherwise FALSE
 */
static gboolean cube_parse_send_device(struct cube *cube,
                                       gpointer     incoming_data)
{
#ifdef DEBUGE_CUBE
  gchar *data = user_data;
  gchar **split = g_strsplit (data, ",", -1);

  g_print ("Duty cycle       : %s\n", split[0]);
  g_print ("Accepted         : %s\n", split[1]);
  g_print ("Free memory slots: %s\n", split[2]);

  g_strfreev (split);
#endif

  return TRUE;
}

/**
 * \brief Parse incoming data
 * \param cube cube pointer
 * \param incoming_data incoming data
 * \return TRUE if metadata has been parsed successfully, otherwise FALSE
 */
static gboolean cube_parse_data(struct cube *cube,
                                gpointer     incoming_data)
{
  gchar *data = incoming_data;
  gboolean ret = FALSE;

  if (data[1] != ':') {
    /* Ooops, something wrong happens here.... return */
    g_warning ("Ooops\n");
    return ret;
  }
#ifdef DEBUG_CUBE_TRAFFIC
  g_print (" ** IN: %s\n", data);
#endif

  switch (data[0]) {
    case INCOMING_HELLO:
      ret = cube_parse_header (cube, data + 2);
      break;
    case INCOMING_METADATA:
      ret = cube_parse_metadata (cube, data + 2);
      break;
    case INCOMING_CONFIGURATION:
      ret = cube_parse_config (cube, data + 2);
      break;
    case INCOMING_DEVICE_LIST:
      ret = cube_parse_device_list (cube, data + 2);
      break;
    case INCOMING_ENCRYPTION:
      ret = cube_parse_encryption (cube, data + 2);
      break;
    case INCOMING_DECRYPTION:
      ret = cube_parse_decryption (cube, data + 2);
      break;
    case INCOMING_NEW_DEVICE:
      ret = cube_parse_new_device (cube, data + 2);
      break;
    case INCOMING_ACKNOWLEDGE:
      ret = TRUE;
      break;
    case INCOMING_SEND_DEVICE_CMD:
      ret = cube_parse_send_device (cube, data + 2);
      break;
    default:
      g_print ("Unhandled header: '%c'\n", data[0]);
      break;
  }

  if (data[0] == cube->wait_for) {
    cube->ack = TRUE;
    cube->wait_for = -1;
    g_cond_signal (&cube->cond);
  }

  return ret;
}

/**
 * \brief Transmit a message to the cube
 * \param cube pointer to cube structure
 * \param packet packet data
 * \param response if set this indicates that we need a response for this packet
 * \return TRUE if handled, FALSE otherwise
 */
static gboolean cube_transmit(struct cube *cube,
                              gchar       *packet,
                              gchar        response)
{
  GError *error = NULL;
  gint64 end_time;

  /* Check if we are still connected */
  if (!g_socket_is_connected (cube->socket)) {
    return FALSE;
  }

  /* Wakeup if needed */
  if (cube->timer && g_timer_elapsed (cube->timer, NULL) > 10000) {
    g_timer_reset (cube->timer);

#ifdef DEBUG_CUBE_TRAFFIC
    g_print ("Wakeup!!\n");
#endif
    cube_send_wakeup (cube);
  }

  g_mutex_lock (&cube->tx_mutex);

#ifdef DEBUG_CUBE_TRAFFIC
  g_print (" ** OUT: %s", packet);
#endif

  if (g_socket_send (cube->socket, packet, strlen (packet), NULL, &error) == -1) {
    g_warning ("Error on transmit. %s\n", error ? error->message : "");
    g_mutex_unlock (&cube->tx_mutex);
    return FALSE;
  }

  if (response) {
    cube->wait_for = response;

    end_time = g_get_monotonic_time () + 5 * G_TIME_SPAN_SECOND;

    while (!cube->ack) {
      if (!g_cond_wait_until (&cube->cond, &cube->tx_mutex, end_time)) {
        g_mutex_unlock (&cube->tx_mutex);
        return FALSE;
      }
    }

    cube->ack = FALSE;
  }

  g_mutex_unlock (&cube->tx_mutex);

  return TRUE;
}

/**
 * \brief Get credentials of the cube
 * \param cube cube device pointer
 * \return TRUE if successful, otherwise FALSE
 */
gboolean cube_get_credentials(struct cube *cube)
{
  gchar *msg;
  gchar *base64;
  gchar *packet;
  gboolean ret;

  msg = g_strdup_printf ("H:%s,%6.6x,%4.4x\r\nG:\r\n", cube->detect_header.serial->str, cube->detect_header.rf_address, cube->detect_header.fw_version);
  base64 = cube_encode ((guchar *)msg, strlen (msg));
  g_free (msg);

  packet = g_strdup_printf ("e:%s\r\n", base64);
  g_free (base64);

  ret = cube_transmit (cube, packet, 0x00);

  g_free (packet);

  return ret;
}

/**
 * \brief Parse encryption
 * \param cube cube pointer
 * \param incoming_data incoming data
 * \return TRUE if metadata has been parsed successfully, otherwise FALSE
 */
static gboolean cube_parse_encryption(struct cube *cube,
                                      gpointer     incoming_data)
{
  gchar *url;
  SoupMessage *msg;
  SoupSession *soup_session_sync;
  gsize len;
  guchar *decoded = cube_decode (incoming_data, &len);
  gboolean ret;

  soup_session_sync = soup_session_new_with_options (SOUP_SESSION_TIMEOUT, 5, NULL);

  url = g_strdup ("http://max.eq-3.de/cube");
  /*url = g_strdup("http://www.tabos.org/cube"); */
  msg = soup_message_new ("POST", url);
  soup_message_set_request (msg, "application/x-www-form-urlencoded",
                            SOUP_MEMORY_TAKE, (gchar *)decoded, len);
  soup_message_headers_append (msg->request_headers, "MAX-Serial", cube->header.serial_number);
  g_free (url);

  soup_session_send_message (soup_session_sync, msg);
  if (msg->status_code != 200 || !msg->response_body->length) {
    g_print ("Received status code: %d", msg->status_code);
    g_print ("Message length: %" G_GOFFSET_FORMAT, msg->response_body->length);
    g_object_unref (msg);

    return FALSE;
  }
  const gchar *data = msg->response_body->data;
  gsize read = msg->response_body->length;

  gchar *base64 = cube_encode ((guchar *)data, read);
  gchar *packet = g_strdup_printf ("d:%s\r\n", base64);
  g_free (base64);

  ret = cube_transmit (cube, packet, INCOMING_DECRYPTION);

  g_free (packet);

  return ret;
}

/**
 * \brief Parse product activation
 * \param cube cube pointer
 * \param incoming_data incoming data
 * \return TRUE if metadata has been parsed successfully, otherwise FALSE
 */
void cube_parse_check_product_activation(struct cube *cube,
                                         gchar      **split)
{
  cube->remaining_days = atoi (split[5]);
  cube->end_date = g_strdup (split[4]);
}

/**
 * \brief Parse credentials
 * \param cube cube pointer
 * \param incoming_data incoming data
 * \return TRUE if metadata has been parsed successfully, otherwise FALSE
 */
void cube_parse_credentials(struct cube *cube,
                            gchar      **split)
{
  cube->user = g_strdup (split[2] + 10 + 2);
  cube->internet_control = strcmp (split[3], "true");
}

/**
 * \brief Parse decryption
 * \param cube cube pointer
 * \param incoming_data incoming data
 * \return TRUE if metadata has been parsed successfully, otherwise FALSE
 */
static gboolean cube_parse_decryption(struct cube *cube,
                                      gpointer     incoming_data)
{
  gsize len;
  guchar *dec = cube_decode (incoming_data, &len);
  gboolean ret = FALSE;
  gchar **split = g_strsplit ((gchar *)dec + 2, ",", -1);
  /*gint i; */

  /*for (i = 0; i < g_strv_length(split); i++) { */
  /*  g_print("%d. %s\n", i, split[i]); */
  /*} */

  switch (split[2][10]) {
    case INCOMING_CHECK_PRODUCT_ACTIVATION:
      /*g_print("=> '%s'\n", split[2] + 10); */
      cube_parse_check_product_activation (cube, split);
      break;
    case INCOMING_GET_CREDENTIALS:
      /*g_print("=> '%s'\n", split[2] + 10); */
      cube_parse_credentials (cube, split);
      break;
    default:
      break;
  }

  return ret;
}

/**
 * \brief Refresh device list
 * \param cube cube device pointer
 * \return TRUE if successful, otherwise FALSE
 */
static gboolean cube_refresh(struct cube *cube)
{
  gchar *packet;
  gboolean ret;

  packet = g_strdup_printf ("l:\r\n");
  ret = cube_transmit (cube, packet, INCOMING_DEVICE_LIST);

  g_free (packet);

  return ret;
}

/**
 * \brief Cube refresh timer called every 10 seconds to gather the status of the cube/devices
 * \param user_data pointer to cube structure
 * \return G_SOURCE_CONTINUE
 */
static gboolean cube_refresh_timer(gpointer user_data)
{
  struct cube *cube = user_data;

  /* Ask for a refresh */
  cube_refresh (cube);

  return G_SOURCE_CONTINUE;
}

/**
 * \brief Cube info timer - registration check, login data
 * \param user_data pointer to cube structure
 * \return NULL
 */
static gboolean cube_info_timer(gpointer user_data)
{
  struct cube *cube = user_data;

  cube_check_product_activation (cube);
  cube_get_credentials (cube);

  return G_SOURCE_REMOVE;
}

/**
 * \brief Cube RX thread which keeps receiving data from the cube and processes the incoming data
 * \param user_data pointer to cube structure
 * \return NULL
 */
static gpointer cube_rx_thread(gpointer user_data)
{
  struct cube *cube = user_data;
  GSocketAddress *remote_address;
  GError *error = NULL;
  gchar incoming[1024];
  gchar buffer[1024];
  gssize len;
  gint buf_offset = 0;
  guint timeout_id;

  /* Create new socket */
  cube->socket = g_socket_new (G_SOCKET_FAMILY_IPV4, G_SOCKET_TYPE_STREAM, G_SOCKET_PROTOCOL_DEFAULT, &error);

  if (cube->detect_header.fw_version >= 0x0109) {
    remote_address = g_inet_socket_address_new (cube->address, MAX_TCP_PORT);
  } else {
    /* TCP Port 80 for fw below 109 */
    remote_address = g_inet_socket_address_new (cube->address, 80);
  }

  /* Connect socket to remote address */
  if (!g_socket_connect (cube->socket, remote_address, NULL, &error)) {
    g_print ("Error on connect\n");
    g_object_unref (remote_address);
    g_object_unref (cube->socket);
    return NULL;
  }

  g_object_unref (remote_address);

  /* Start refresh timer every 10 seconds */
  timeout_id = g_timeout_add_seconds (10, cube_refresh_timer, cube);

  /* Start information gathering */
  g_timeout_add_seconds (3, cube_info_timer, cube);

  /* Create cube activity timer */
  cube->timer = g_timer_new ();

  /* Ensure that the last byte is terminated */
  incoming[1023] = '\0';

  do {
    gchar *pos;
    gint offset = 0;

    /* Read data */
    len = g_socket_receive_with_blocking (cube->socket, incoming, sizeof (incoming) - 1, TRUE, NULL, &error);
    if (len == -1) {
      /* In case of an error, reset buffer offset */
      buf_offset = 0;
      continue;
    }

    /* Reset activity timer */
    g_timer_reset (cube->timer);

    do {
      pos = g_strstr_len (incoming + offset, len - offset, "\r\n");
      if (pos) {
        gint l = pos - (incoming + offset);

        memcpy (buffer + buf_offset, incoming + offset, l);
        buffer[buf_offset + l] = '\0';
        offset += l + 2;

        cube_parse_data (cube, buffer);

        buf_offset = 0;
      } else {
        gint l = len - offset;

        memcpy (buffer, incoming + offset, l);
        buf_offset = l;

        offset = len;
      }
    } while ((len - offset) > 0);
  } while (!g_socket_is_closed (cube->socket));

  /* Cleanup */
  g_source_remove (timeout_id);
  g_timer_destroy (cube->timer);
  g_object_unref (cube->socket);

  return NULL;
}

/**
 * \brief Start cube rx thread
 * \param cube pointer to cube structure
 */
void cube_start_thread(struct cube *cube)
{
  /* Start rx thread */
  g_thread_new ("cube rx thread", cube_rx_thread, cube);
}

/**
 * \brief Check product activateion (NEEDS WORK)
 * \param cube cube pointer
 * \return TRUE if activated, otherwise FALSE
 */
gboolean cube_check_product_activation(struct cube *cube)
{
  gchar *msg;
  gchar *base64;
  gchar *packet;
  gboolean ret;

  msg = g_strdup_printf ("H:%s,%6.6x,%4.4x\r\nV:\r\n", cube->detect_header.serial->str, cube->detect_header.rf_address, cube->detect_header.fw_version);
  base64 = cube_encode ((guchar *)msg, strlen (msg));
  g_free (msg);

  packet = g_strdup_printf ("e:%s\r\n", base64);
  g_free (base64);

  ret = cube_transmit (cube, packet, 0x00);

  g_free (packet);

  return ret;
}

/**
 * \brief Start device scan
 * \param cube cube pointer
 * \return TRUE if started, otherwise FALSE
 */
gboolean cube_start_device_scan(struct cube *cube)
{
  gchar *packet;
  gboolean ret;

  packet = g_strdup_printf ("n:003c\r\n");
  ret = cube_transmit (cube, packet, 0x00);
  g_free (packet);

  return ret;
}

/**
 * \brief Stop device scan
 * \param cube cube pointer
 * \return TRUE if stopped, otherwise FALSE
 */
gboolean cube_stop_device_scan(struct cube *cube)
{
  gchar *packet;
  gboolean ret;

  packet = g_strdup_printf ("x:\r\n");
  ret = cube_transmit (cube, packet, INCOMING_NEW_DEVICE);
  g_free (packet);

  return ret;
}

/**
 * \brief Send cube meta data (room information including devices)
 * \param cube cube pointer
 * \return TRUE if successful, otherwise FALSE
 */
gboolean cube_send_meta(struct cube *cube)
{
  GString *msg;
  gchar *base64;
  gchar *packet;
  GList *list;
  gboolean ret;

  msg = g_string_new ("");
  g_string_append_printf (msg, "%c%c%c",
                          0x56,
                          0x02,
                          g_list_length (cube->rooms));

  for (list = cube->rooms; list != NULL; list = list->next) {
    struct cube_room *room = list->data;

    g_string_append_printf (msg, "%c%c%s%c%c%c",
                            room->id,
                            (gchar)room->name->len,
                            room->name->str,
                            (room->rf_address >> 16) & 0xFF,
                            (room->rf_address >> 8) & 0xFF,
                            room->rf_address & 0xFF);
  }

  g_string_append_printf (msg, "%c",
                          g_list_length (cube->devices));

  for (list = cube->devices; list != NULL; list = list->next) {
    struct cube_device *device = list->data;

    g_string_append_printf (msg, "%c%c%c%c%s%c%s%c",
                            device->type,
                            (device->rf_address >> 16) & 0xFF,
                            (device->rf_address >> 8) & 0xFF,
                            device->rf_address & 0xFF,
                            device->serial_number->str,
                            device->name ? (gchar)device->name->len : 0,
                            device->name ? device->name->str : "",
                            device->room_id);
  }

  g_string_append_printf (msg, "%c",
                          0x01);

  base64 = cube_encode ((guchar *)msg->str, msg->len);
  g_string_free (msg, TRUE);

  packet = g_strdup_printf ("m:00,%s\r\n", base64);
  g_free (base64);

  ret = cube_transmit (cube, packet, INCOMING_ACKNOWLEDGE);

  g_free (packet);

  return ret;
}

/**
 * \brief Send base device config (temperatures)
 * \param cube cube pointer
 * \param cube_device cube device pointer
 * \return TRUE if successful, otherwise FALSE
 */
gboolean cube_send_device_base_config(struct cube        *cube,
                                      struct cube_device *device)
{
  guchar msg[17];
  gchar *base64;
  gchar *packet;
  gboolean ret;

  /* Command */
  msg[0] = 0x00;
  msg[1] = 0x00;
  msg[2] = CUBE_CMD_CONFIG_TEMPERATURE;
  msg[3] = 0x00;
  msg[4] = 0x00;
  msg[5] = 0x00;

  /* RF address */
  msg[6] = (device->rf_address >> 16) & 0xFF;
  msg[7] = (device->rf_address >> 8) & 0xFF;
  msg[8] = device->rf_address & 0xFF;

  /* Room id */
  msg[9] = device->room_id;

  /* Comfort temp */
  msg[10] = (gint)((gfloat)device->config->comfort_temp * 2.0f);
  /* Eco temp */
  msg[11] = (gfloat)device->config->eco_temp * 2.0f;
  /* Max set point temp */
  msg[12] = (gfloat)device->config->max_set_point_temp * 2.0f;
  /* Min set point temp */
  msg[13] = (gfloat)device->config->min_set_point_temp * 2.0f;
  /* Temp offset */
  msg[14] = (gint)((gfloat)(device->config->temp_offset + 3.5f) * 2.0f);
  g_print ("%.1f %1.f %1.f %d\n",
           device->config->temp_offset,
           device->config->temp_offset + 3.5f,
           (device->config->temp_offset + 3.5f) * 2.0f,
           msg[14]);
  /* Window open temp */
  msg[15] = (gfloat)device->config->window_open_temp * 2.0f;
  /* Window open duration */
  msg[16] = (gfloat)device->config->window_open_duration;

  base64 = cube_encode (msg, sizeof (msg));

  packet = g_strdup_printf ("s:%s\r\n", base64);
  g_free (base64);

  ret = cube_transmit (cube, packet, INCOMING_SEND_DEVICE_CMD);

  g_free (packet);

  return ret;
}

/**
 * \brief Wall thermostat: set display
 * \param cube cube pointer
 * \param cube_device cube device pointer
 * \return TRUE if successful, otherwise FALSE
 */
gboolean cube_set_wall_display(struct cube        *cube,
                               struct cube_device *device,
                               gint                value)
{
  guchar msg[11];
  gchar *base64;
  gchar *packet;
  gboolean ret;

  /* Command */
  msg[0] = 0x00;
  msg[1] = 0x00;
  msg[2] = CUBE_CMD_SET_DISPLAY_ACTUAL_TEMP;
  msg[3] = 0x00;
  msg[4] = 0x00;
  msg[5] = 0x00;

  /* RF address */
  msg[6] = (device->rf_address >> 16) & 0xFF;
  msg[7] = (device->rf_address >> 8) & 0xFF;
  msg[8] = device->rf_address & 0xFF;

  msg[9] = 0x00;
  msg[10] = value ? 0x04 : 0x00;
  base64 = cube_encode (msg, sizeof (msg));

  packet = g_strdup_printf ("s:%s\r\n", base64);
  g_free (base64);

  ret = cube_transmit (cube, packet, INCOMING_SEND_DEVICE_CMD);

  g_free (packet);

  return ret;
}

/**
 * \brief Set device temperature/mode
 * \param cube cube pointer
 * \param cube_device cube device pointer
 * \param mode device mode
 * \param temperature set temperature
 * \param until_date date until this temperature should be set (depends on selected device mode)
 * \param until_time time until this temperature should be set (depends on selected device mode)
 * \return TRUE if successful, otherwise FALSE
 */
gboolean cube_set_device_temperature(struct cube        *cube,
                                     struct cube_device *device,
                                     eCubeMode           mode,
                                     gfloat              temperature,
                                     gint                until_date,
                                     gint                until_time)
{
  guchar msg[14];
  gchar *base64;
  gchar *packet;
  gint len = 11;
  gboolean ret;

  /* Command */
  msg[0] = 0x00;
  msg[1] = 0x04;
  msg[2] = CUBE_CMD_SET_TEMPERATURE;
  msg[3] = 0x00;
  msg[4] = 0x00;
  msg[5] = 0x00;

  /* RF address */
  msg[6] = (device->rf_address >> 16) & 0xFF;
  msg[7] = (device->rf_address >> 8) & 0xFF;
  msg[8] = device->rf_address & 0xFF;

  /* Room id */
  msg[9] = device->room_id;

  msg[10] = ((gint)((temperature * 2.0f)) & 0x3F) + (mode << 0x06);

  g_print ("mode: %d temperature: %.1f\n", mode, temperature);

  if (mode == CUBE_MODE_VACATION) {
    gint year;
    gint month;
    gint day;

    year = (until_date & 0x1F) + 2000;
    month = ((until_date & 0xE000) >> 12) + ((until_date >> 6) & 0x1);
    day = (until_date >> 8) & 0x1F;

    g_print ("year: %d, month: %d, day: %d\n", year, month, day);

    msg[11] = (until_date >> 8) & 0xFF;
    msg[12] = until_date & 0xFF;

    msg[13] = until_time & 0xFF;

    len = 14;
  }

  base64 = cube_encode (msg, len);
  packet = g_strdup_printf ("s:%s\r\n", base64);
  g_free (base64);

  ret = cube_transmit (cube, packet, INCOMING_SEND_DEVICE_CMD);

  g_free (packet);

  return ret;
}

/**
 * \brief Set device link partner
 * \param cube cube pointer
 * \param cube_device cube device pointer
 * \param partner partner device pointer
 * \param master indicates that device is master
 * \return TRUE if successful, otherwise FALSE
 */
static gboolean cube_device_set_link_partner(struct cube        *cube,
                                             struct cube_device *device,
                                             struct cube_device *partner,
                                             gboolean            master)
{
  guchar msg[14];
  gchar *base64;
  gchar *packet;
  gboolean ret;

  /* Command */
  msg[0] = 0x00;
  msg[1] = master ? 0x04 : 0x00;
  msg[2] = CUBE_CMD_ADD_LINK_PARTNER;
  msg[3] = 0x00;
  msg[4] = 0x00;
  msg[5] = 0x00;

  /* RF address */
  msg[6] = (device->rf_address >> 16) & 0xFF;
  msg[7] = (device->rf_address >> 8) & 0xFF;
  msg[8] = device->rf_address & 0xFF;

  /* Room id */
  msg[9] = master ? device->room_id : 0x00;

  /* RF partner address */
  msg[10] = (partner->rf_address >> 16) & 0xFF;
  msg[11] = (partner->rf_address >> 8) & 0xFF;
  msg[12] = partner->rf_address & 0xFF;

  msg[13] = partner->type;

  base64 = cube_encode (msg, sizeof (msg));
  packet = g_strdup_printf ("s:%s\r\n", base64);
  g_free (base64);

  ret = cube_transmit (cube, packet, INCOMING_SEND_DEVICE_CMD);

  g_free (packet);

  return ret;
}

/**
 * \brief Unlink device partner
 * \param cube cube pointer
 * \param cube_device cube device pointer
 * \param partner partner device pointer
 * \param master indicates that device is master
 * \return TRUE if successful, otherwise FALSE
 */
static gboolean cube_device_set_unlink_partner(struct cube        *cube,
                                               struct cube_device *device,
                                               struct cube_device *partner,
                                               gboolean            master)
{
  guchar msg[14];
  gchar *base64;
  gchar *packet;
  gboolean ret;

  /* Command */
  msg[0] = 0x00;
  msg[1] = master ? 0x04 : 0x00;
  msg[2] = CUBE_CMD_REMOVE_LINK_PARTNER;
  msg[3] = 0x00;
  msg[4] = 0x00;
  msg[5] = 0x00;

  /* RF address */
  msg[6] = (device->rf_address >> 16) & 0xFF;
  msg[7] = (device->rf_address >> 8) & 0xFF;
  msg[8] = device->rf_address & 0xFF;

  /* Room id */
  msg[9] = master ? device->room_id : 0x00;

  /* RF partner address */
  msg[10] = (partner->rf_address >> 16) & 0xFF;
  msg[11] = (partner->rf_address >> 8) & 0xFF;
  msg[12] = partner->rf_address & 0xFF;

  msg[13] = partner->type;

  base64 = cube_encode (msg, sizeof (msg));
  packet = g_strdup_printf ("s:%s\r\n", base64);
  g_free (base64);

  ret = cube_transmit (cube, packet, INCOMING_SEND_DEVICE_CMD);

  g_free (packet);

  return ret;
}

/**
 * \brief Add new device link partner to a room
 * \param cube cube pointer
 * \param device new cube device pointer
 * \return TRUE if successful, otherwise FALSE
 */
static gboolean cube_room_add_link_partner(struct cube_room   *room,
                                           struct cube_device *device)
{
  GList *list = room->devices;
  gboolean ret = TRUE;

  if (!g_list_length (list)) {
    return TRUE;
  }

  cube_device_set_link_partner (room->cube, list->data, device, TRUE);

  for (; list != NULL; list = list->next) {
    if (!cube_device_set_link_partner (room->cube, device, list->data, FALSE)) {
      ret = FALSE;
    }
  }

  return ret;
}

/**
 * \brief Rename room name
 * \param cube pointer to cube structure
 * \param room room structure
 * \param name new name
 * \return TRUE if successful, otherwise FALSE
 */
gboolean cube_rename_room(struct cube      *cube,
                          struct cube_room *room,
                          const gchar      *name)
{
  gboolean ret;

  g_string_printf (room->name, "%s", name);

  ret = cube_send_meta (cube);

  return ret;
}

/**
 * \brief Rename device name
 * \param cube pointer to cube structure
 * \param device pointer to cube device structure
 * \param name new name
 * \return TRUE if successful, otherwise FALSE
 */
gboolean cube_rename_device(struct cube        *cube,
                            struct cube_device *device,
                            const gchar        *name)
{
  gboolean ret;

  g_string_printf (device->name, "%s", name);

  ret = cube_send_meta (cube);
  if (ret) {
    cube_refresh (cube);
  }

  return ret;
}

/**
 * \brief Remove device from room
 * \param room pointer to cube room structure
 * \param device device to remove from room
 */
void cube_room_remove_device(struct cube_room   *room,
                             struct cube_device *device)
{
  struct cube_device *master = cube_get_device_by_address (room->cube, room->rf_address);
  guchar msg[3];
  gchar *base64;
  gchar *packet;

  if (master && master->rf_address != device->rf_address) {
    cube_device_set_unlink_partner (room->cube, master, device, TRUE);
  } else {
    g_print ("[%s]: TODO\n", __FUNCTION__);
  }

  /* RF address */
  msg[0] = (device->rf_address >> 16) & 0xFF;
  msg[1] = (device->rf_address >> 8) & 0xFF;
  msg[2] = device->rf_address & 0xFF;

  base64 = cube_encode (msg, sizeof (msg));

  packet = g_strdup_printf ("t:01,1,%s\r\n", base64);
  g_free (base64);

  cube_transmit (room->cube, packet, INCOMING_ACKNOWLEDGE);

  g_free (packet);

  room->cube->devices = g_list_remove (room->cube->devices, device);
  room->devices = g_list_remove (room->devices, device);

  cube_send_meta (room->cube);
}

/**
 * \brief Remove room name
 * \param cube pointer to cube structure
 * \param room room structure
 * \return TRUE if successful, otherwise FALSE
 */
gboolean cube_remove_room(struct cube      *cube,
                          struct cube_room *room)
{
  gboolean ret;

  while (room->devices) {
    cube_room_remove_device (room, room->devices->data);
  }

  cube->rooms = g_list_remove (cube->rooms, room);

  ret = cube_send_meta (cube);

  return ret;
}

/**
 * \brief Request device config
 * \param cube pointer to cube structure
 * \param device device structure
 * \return TRUE if successful, otherwise FALSE
 */
gboolean cube_device_request_config(struct cube        *cube,
                                    struct cube_device *device)
{
  gchar *packet;
  gboolean ret;

  /* Request configuration */
  packet = g_strdup_printf ("c:%6.6x\r\n", device->rf_address);
  ret = cube_transmit (cube, packet, INCOMING_CONFIGURATION);
  g_free (packet);

  return ret;
}

/**
 * \brief Send device week program
 * \param cube pointer to cube structure
 * \param device device structure
 * \return TRUE if successful, otherwise FALSE
 */
gboolean cube_send_device_week_program(struct cube        *cube,
                                       struct cube_device *device)
{
  gint days;
  gboolean ret = TRUE;

  /* Weekly program (Sat, Sun, Mon, Tue, Weg, Thu, Fri) */
  for (days = 0; days < 7; days++) {
    guchar msg[25];
    gchar *base64;
    gchar *packet;
    gint i;
    GList *list;

#ifdef DEBUG_CUBE_CONFIG_DEVICE
    g_print (" Day %d               : ", days);

    for (list = device->config->days[days]; list != NULL; list = list->next) {
      struct cube_day_entry *entry = list->data;

      g_print ("%.1fC -> %2.2d:%2.2d", entry->deg, entry->min * 5 / 60, entry->min * 5 % 60);

      /* We'll skip the output as we have reached the end of the day */
      if ((entry->min * 5 / 60) == 24) {
        break;
      }

      if (list->next) {
        g_print (", ");
      }
    }
    g_print ("\n");
#endif

    /* Command */
    msg[0] = 0x00;
    msg[1] = 0x00;
    msg[2] = CUBE_CMD_CONFIG_WEEK_PROFILE;
    msg[3] = 0x00;
    msg[4] = 0x00;
    msg[5] = 0x00;

    /* RF address */
    msg[6] = (device->rf_address >> 16) & 0xFF;
    msg[7] = (device->rf_address >> 8) & 0xFF;
    msg[8] = device->rf_address & 0xFF;

    /* Not used */
    msg[9] = 0x00;

    /* Room id */
    msg[10] = device->room_id;

    i = 11;
    for (list = device->config->days[days]; list != NULL; list = list->next) {
      struct cube_day_entry *entry = list->data;
      gint val;

      val = ((gint)(entry->deg * 2.0f) << 9) + entry->min;

      msg[i++] = (val >> 8) & 0xFF;
      msg[i++] = val & 0xFF;
    }

    base64 = cube_encode (msg, sizeof (msg));
    packet = g_strdup_printf ("s:%s\r\n", base64);
    g_free (base64);

    if (!cube_transmit (cube, packet, INCOMING_SEND_DEVICE_CMD)) {
      ret = FALSE;
    }

    g_free (packet);
  }

  return ret;
}

/**
 * \brief Send device day program
 * \param cube pointer to cube structure
 * \param device device structure
 * \return TRUE if successful, otherwise FALSE
 */
gboolean cube_send_device_day_program(struct cube        *cube,
                                      struct cube_device *device,
                                      gint                day,
                                      GList              *program)
{
  gboolean ret = TRUE;
  guchar msg[25];
  gchar *base64;
  gchar *packet;
  gint i;
  GList *list;

#ifdef DEBUG_CUBE_CONFIG_DEVICE
  g_print (" Day %d               : ", day);

  for (list = program; list != NULL; list = list->next) {
    struct cube_day_entry *entry = list->data;

    g_print ("%.1fC -> %2.2d:%2.2d", entry->deg, entry->min * 5 / 60, entry->min * 5 % 60);

    /* We'll skip the output as we have reached the end of the day */
    if ((entry->min * 5 / 60) == 24) {
      break;
    }

    if (list->next) {
      g_print (", ");
    }
  }
  g_print ("\n");
#endif

  /* Command */
  msg[0] = 0x00;
  msg[1] = 0x04;
  msg[2] = CUBE_CMD_CONFIG_WEEK_PROFILE;
  msg[3] = 0x00;
  msg[4] = 0x00;
  msg[5] = 0x00;

  /* RF address */
  msg[6] = (device->rf_address >> 16) & 0xFF;
  msg[7] = (device->rf_address >> 8) & 0xFF;
  msg[8] = device->rf_address & 0xFF;

  /* Room id */
  msg[9] = device->room_id;

  /* Day */
  msg[10] = day;

  i = 11;
  for (list = program; list != NULL; list = list->next) {
    struct cube_day_entry *entry = list->data;
    gint val;

    val = ((gint)(entry->deg * 2.0f) << 9) + entry->min;

    msg[i++] = (val >> 8) & 0xFF;
    msg[i++] = val & 0xFF;
  }

  base64 = cube_encode (msg, sizeof (msg));
  packet = g_strdup_printf ("s:%s\r\n", base64);
  g_free (base64);

  if (!cube_transmit (cube, packet, INCOMING_SEND_DEVICE_CMD)) {
    ret = FALSE;
  }

  g_free (packet);

  return ret;
}

/**
 * \brief Send wakeup
 * \param cube pointer to cube structure
 * \return TRUE if successful, otherwise FALSE
 */
gboolean cube_send_wakeup(struct cube *cube)
{
  gchar packet[255];

  strcpy (packet, "z:1e,G,01");

  return cube_transmit (cube, packet, INCOMING_ACKNOWLEDGE);
}

/**
 * \brief Add new room to cube
 * \param cube pointer to cube structure
 * \param name name of new room
 * \return TRUE if successful, otherwise FALSE
 */
gboolean cube_add_room(struct cube *cube,
                       const gchar *name)
{
  struct cube_room *room = g_slice_alloc0 (sizeof (struct cube_room));

  room->id = cube->rooms ? (g_list_length (cube->rooms) + 1) : 0;
  room->name = g_string_new (name);

  cube->rooms = g_list_append (cube->rooms, room);

  return cube_send_meta (cube);
}

/**
 * \brief Get room by name
 * \param cube pointer to cube structure
 * \param name name of room
 * \return pointer to cube room or NULL if not found
 */
struct cube_room *cube_get_room_by_name(struct cube *cube,
                                        const gchar *name)
{
  GList *list;

  for (list = cube->rooms; list != NULL; list = list->next) {
    struct cube_room *room = list->data;

    if (!strcmp (room->name->str, name)) {
      return room;
    }
  }

  return NULL;
}

/**
 * \brief Get room by id
 * \param cube pointer to cube structure
 * \param id id of room
 * \return pointer to cube room or NULL if not found
 */
struct cube_room *cube_get_room_by_id(struct cube *cube,
                                      gint         id)
{
  GList *list;

  for (list = cube->rooms; list != NULL; list = list->next) {
    struct cube_room *room = list->data;

    if (room->id == id) {
      return room;
    }
  }

  return NULL;
}

/**
 * \brief Get device by address
 * \param cube pointer to cube structure
 * \param rf_address rf address
 * \return pointer to cube device or NULL if not found
 */
struct cube_device *cube_get_device_by_address(struct cube *cube,
                                               gint         rf_address)
{
  GList *list;

  for (list = cube->devices; list != NULL; list = list->next) {
    struct cube_device *device = list->data;

    if (device->rf_address == rf_address) {
      return device;
    }
  }

  return NULL;
}

/**
 * \brief Set base config for every device in selected room
 * \param cube room pointer to cube room structure
 * \param eco eco temperature
 * \param comfor comfort temperature
 * \param window window open temperature
 * \param max_set_point max set point temperature
 * \return TRUE if successful, otherwise FALSE
 */
gboolean cube_room_set_base_config(struct cube_room *room,
                                   gfloat            eco,
                                   gfloat            comfort,
                                   gfloat            window,
                                   gfloat            max_set_point)
{
  GList *list;
  gboolean ret = TRUE;

  for (list = room->devices; list != NULL; list = list->next) {
    struct cube_device *device = list->data;

    if (device->type != CUBE_DEVICE_TYPE_HEATING) {
      continue;
    }

    device->config->eco_temp = eco;
    device->config->comfort_temp = comfort;
    device->config->window_open_temp = window;
    device->config->max_set_point_temp = max_set_point;

    if (!cube_send_device_base_config (room->cube, device)) {
      ret = FALSE;
    }
  }

  return ret;
}

/**
 * \brief Get free room id
 * \param cube pointer to cube structure
 * \return free room id
 */
gint cube_get_free_room_id(struct cube *cube)
{
  gint i = 1;
  gboolean found;

  do {
    GList *rooms;

    found = TRUE;

    for (rooms = cube->rooms; rooms != NULL; rooms = rooms->next) {
      struct cube_room *room = rooms->data;

      if (room->id == i) {
        found = FALSE;
        i++;
      }
    }
  } while (!found);

  return i;
}

/**
 * \brief Create new room structure
 * \param cube pointer to cube structure
 * \param name name of new room
 * \return new cube room structure
 */
struct cube_room *cube_create_room(struct cube *cube,
                                   const gchar *name)
{
  struct cube_room *room = g_slice_alloc0 (sizeof (struct cube_room));

  room->id = cube_get_free_room_id (cube);
  room->rf_address = 0x000000;
  room->name = g_string_new (name);
  room->cube = cube;

  cube->rooms = g_list_append (cube->rooms, room);

  return room;
}

/**
 * \brief Propose device name
 * \param cube pointer to cube structure
 * \param device cube device structure
 * \return proposed device name
 */
gchar *cube_propose_device_name(struct cube        *cube,
                                struct cube_device *device)
{
  GList *list = NULL;
  const gchar *pre = cube_device_string (device->type);
  gint index = 1;
  gchar *name = NULL;

  do {
    name = g_strdup_printf ("%s %d", pre, index);

    for (list = cube->devices; list != NULL; list = list->next) {
      struct cube_device *dev = list->data;

      if (dev->name && !strcmp (dev->name->str, name)) {
        index++;
        g_free (name);
        name = NULL;
        break;
      }
    }
  } while (!name);

  return name;
}

/**
 * \brief Retrieve current mode of selected device
 * \param device cube device structure
 * \return cube device mode
 */
eCubeMode cube_device_get_mode(struct cube_device *device)
{
  return device ? device->status2 & 0x3 : CUBE_MODE_MANUAL;
}

/**
 * \brief Check whether battery is low
 * \param device cube device structure
 * \return TRUE if battery is low, otherwise FALSE
 */
gboolean cube_device_is_battery_low(struct cube_device *device)
{
  return device ? device->status2 & 0x80 : TRUE;
}

/**
 * \brief Send device config
 * \param device cube device structure
 * \return cube device mode
 */
static void cube_send_device_config(struct cube        *cube,
                                    struct cube_device *device)
{
  cube_send_device_base_config (cube, device);

  if (device->type == CUBE_DEVICE_TYPE_WALL) {
    cube_send_device_week_program (cube, device);
  }
}

/**
 * \brief Set config to all devices in the room
 * \param room cube room structure
 * \param device new device
 */
static void cube_room_set_config(struct cube_room   *room,
                                 struct cube_device *device)
{
  GList *list = room->devices;

  /* Resend all device configs */
  for (list = room->devices; list != NULL; list = list->next) {
    struct cube_device *dev = list->data;

    if (dev->type != CUBE_DEVICE_TYPE_SHUTTER) {
      g_print ("Resending config for: %s/%s\n", dev->serial_number->str, cube_device_string (dev->type));
      cube_send_device_config (room->cube, dev);
    }
  }

  /* Set new device twice.... */
  cube_send_device_config (room->cube, device);
  /*cube_send_device_config(room->cube, device); */
}

/**
 * \brief Set boost and valve for the room
 * \param room cube room structure
 * \param boost boost time in minutes
 * \param valve valve in percent
 * \param decalc_day decalc day
 * \param decalc_hour decalc hour
 * \param max max valve setting
 * \param offset valve offset
 * \return TRUE if successful, otherwise FALSE
 */
gboolean cube_room_config_valve(struct cube_room *room,
                                gint              boost,
                                gint              valve,
                                gint              decalc_day,
                                gint              decalc_hour,
                                gint              max,
                                gint              offset)
{
  struct cube_device *device = cube_room_get_device (room, CUBE_DEVICE_TYPE_HEATING);
  guchar msg[14];
  gchar *base64;
  gchar *packet;
  gboolean ret;

  /* Command */
  msg[0] = 0x00;
  msg[1] = 0x04;
  msg[2] = CUBE_CMD_CONFIG_VALVE;
  msg[3] = 0x00;
  msg[4] = 0x00;
  msg[5] = 0x00;

  /* RF address */
  msg[6] = (device->rf_address >> 16) & 0xFF;
  msg[7] = (device->rf_address >> 8) & 0xFF;
  msg[8] = device->rf_address & 0xFF;

  /* Room id */
  msg[9] = room->id;

  /* Boost */
  /*duration = config->boost_duration_val >> 5; */
  /*value = config->boost_duration_val & 0x1F; */
  /*g_print(" boost duration (%d) : duration = %dmin, val = %d%%\n", config->boost_duration_val, duration == 7 ? 30 : duration * 5, value * 5); */

  if (boost == 30) {
    boost = 7;
  } else {
    boost /= 5;
  }

  valve /= 5;

  g_print ("device->config->boost_duration_val: %2.2x\n", device->config->boost_duration_val);
  g_print ("boost: %x %2.2x\n", boost, boost << 5);
  g_print ("val: %x %x\n", valve, valve);
  g_print (" ==> %x\n", (boost << 5) + valve);

  msg[10] = (boost << 5) + valve;

  /*day = config->decalcification >> 5; */
  /*hour = config->decalcification & 0x1F; */
  g_print ("device->config->decalcification: %2.2x\n", device->config->decalcification);
  g_print ("day: %x %2.2x\n", decalc_day, decalc_day << 5);
  g_print ("hour: %x %x\n", decalc_hour, decalc_hour);
  g_print (" ==> %2.2x\n", (decalc_day << 5) + decalc_hour);
  msg[11] = (decalc_day << 5) + decalc_hour;

  msg[12] = device->config->max_val_setting;
  msg[13] = device->config->val_offset;

  base64 = cube_encode (msg, sizeof (msg));
  packet = g_strdup_printf ("s:%s\r\n", base64);
  g_free (base64);

  ret = cube_transmit (room->cube, packet, INCOMING_SEND_DEVICE_CMD);
  ret = FALSE;

  g_free (packet);

  return ret;
}

/**
 * \brief Set group rf address for the room
 * \param room cube room structure
 * \param device new device in room
 * \return TRUE if successful, otherwise FALSE
 */
static gboolean cube_room_set_group_address(struct cube_room   *room,
                                            struct cube_device *device)
{
  guchar msg[11];
  gchar *base64;
  gchar *packet;
  gboolean ret;

  /* Command */
  msg[0] = 0x00;
  msg[1] = 0x00;
  msg[2] = CUBE_CMD_SET_GROUP_ID;
  msg[3] = 0x00;
  msg[4] = 0x00;
  msg[5] = 0x00;

  /* RF address */
  msg[6] = (device->rf_address >> 16) & 0xFF;
  msg[7] = (device->rf_address >> 8) & 0xFF;
  msg[8] = device->rf_address & 0xFF;

  /* Not used */
  msg[9] = 0x00;

  /* Room id */
  msg[10] = room->id;

  base64 = cube_encode (msg, sizeof (msg));
  packet = g_strdup_printf ("s:%s\r\n", base64);
  g_free (base64);

  ret = cube_transmit (room->cube, packet, INCOMING_SEND_DEVICE_CMD);

  g_free (packet);

  return ret;
}

/**
 * \brief Set mode in room
 * \param room cube room structur
 * \param device cube device structure
 */
static gboolean cube_room_set_mode(struct cube_room   *room,
                                   struct cube_device *device)
{
  struct cube_device *master;

  if (!g_list_length (room->devices)) {
    return FALSE;
  }

  master = room->devices->data;

  return cube_set_device_temperature (room->cube, device, master->status2 & 0x03, master->set_temp, 0, 0);
}

/**
 * \brief Add new device to room (set link partner, set group address, set config and mode, update metadata)
 * \param room cube room structure
 * \param device cube device structure
 */
void cube_room_add_device(struct cube_room   *room,
                          struct cube_device *device)
{
  g_print ("room id: %d\n", device->room_id);
  device->room_id = room->id;
  g_print ("room id: %d\n", device->room_id);

  g_print ("set link partner\n");
  cube_room_add_link_partner (room, device);
  g_print ("set group address\n");
  cube_room_set_group_address (room, device);
  g_print ("request config\n");
  cube_device_request_config (room->cube, device);

  if (device->type != CUBE_DEVICE_TYPE_SHUTTER) {
    g_print ("set config\n");
    cube_room_set_config (room, device);

    if (device->type != CUBE_DEVICE_TYPE_WALL) {
      g_print ("set mode\n");
      cube_room_set_mode (room, device);
    }
  }

  room->devices = g_list_append (room->devices, device);
  cube_send_meta (room->cube);
}

/**
 * \brief Get selected device of the given room
 * \param room cube room structure
 * \param type cube device type
 * \return heater cube device or NULL if not found
 */
struct cube_device *cube_room_get_device(struct cube_room *room,
                                         eCubeDeviceType   type)
{
  GList *list;
  struct cube_device *heater = NULL;

  for (list = room->devices; list != NULL; list = list->next) {
    struct cube_device *dev = list->data;

    if (dev->type == type) {
      heater = dev;
      break;
    }
  }

  return heater;
}

/**
 * \brief Set active cube
 * \param cube pointer to cube structure
 */
void cube_set_active(struct cube *cube)
{
  active_cube = cube;
}

/**
 * \brief Get active cube
 * \return cube pointer to cube structure
 */
struct cube *cube_get_active(void)
{
  return active_cube;
}

/**
 * \brief Initialize cube system
 */
void cube_init(void)
{
  cube_object = cube_object_new ();
}

/**
 * \brief Shutdown cube
 * \param cube pointer to cube structure
 * \return TRUE if successful, otherwise FALSE
 */
gboolean cube_shutdown(struct cube *cube)
{
  gchar *packet;
  GError *error = NULL;
  gboolean ret;

  packet = g_strdup_printf ("q:\r\n");

  ret = cube_transmit (cube, packet, 0);

  g_socket_close (cube->socket, &error);
  g_free (packet);

  return ret;
}

/**
 * Max Control
 * Copyright (c) 2015-2016 Jan-Michael Brummer
 *
 * This file is part of Max Control.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 only.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>

#include <string.h>

#include <gtk/gtk.h>

#include <cube.h>
#include <cube_object.h>
#include <max.h>
#include <about.h>

/** Main window of max control */
static GtkWidget *max_main_window = NULL;
/** This is the box showing new devices on scan request */
static GtkWidget *max_new_device_box = NULL;
/** Flowbox containing the different rooms connected to the cube */
static GtkWidget *max_room_flowbox = NULL;
/** Exclusive ui mutex */
static GMutex max_ui_mutex;
static GList *week_days[7];
static GtkWidget *stack;

struct cmd_line_option_state {
  gchar *ip;
};

static struct cmd_line_option_state option_state;

struct room_ui {
  GtkWidget *name;
  GtkWidget *temperature;
  GtkWidget *mode;
  GtkWidget *note;
};

/**
 * \brief About activated - show about dialog
 * \param action simple action
 * \param parameter variant parameter (UNUSED)
 * \param user_data pointer to GtkApplicaton
 */
static void about_activated(GSimpleAction *action,
                            GVariant      *parameter,
                            gpointer       user_data)
{
  /* Show about dialog */
  app_show_about ();
}

/**
 * \brief Quit activated - shutdown current cube and exit application
 * \param action simple action
 * \param parameter variant parameter (UNUSED)
 * \param user_data pointer to GtkApplication
 */
static void quit_activated(GSimpleAction *action,
                           GVariant      *parameter,
                           gpointer       user_data)
{
  GtkApplication *app = user_data;

  /* Shutdown active cube */
  cube_shutdown (cube_get_active ());
  /* Quit application */
  g_application_quit (G_APPLICATION (app));
}

/**
 * \brief Create a new ui label (grey, end aligned)
 * \param text text of new label
 * \return label widget
 */
GtkWidget *ui_label_new(gchar *text)
{
  GtkWidget *label;

  label = gtk_label_new (text);

  /* Set grey color */
  gtk_widget_set_sensitive (label, FALSE);
  /* Align at end */
  gtk_widget_set_halign (label, GTK_ALIGN_END);

  return label;
}

void flowbox_delete(GtkWidget *widget,
                    gpointer   user_data)
{
  gtk_widget_destroy (widget);
}

void max_decrease_temp(GtkWidget *widget,
                       gpointer   user_data)
{
  struct cube_device *device = user_data;
  struct cube *cube = cube_get_active ();

  cube_set_device_temperature (cube, device, cube_device_get_mode (device), device->set_temp - 0.5f, 0, 0);
}

void max_increase_temp(GtkWidget *widget,
                       gpointer   user_data)
{
  struct cube_device *device = user_data;

  cube_set_device_temperature (cube_get_active (), device, cube_device_get_mode (device), device->set_temp + 0.5f, 0, 0);
}

void max_mode_switch(GtkWidget *widget,
                     gboolean   state,
                     gpointer   user_data)
{
  struct cube_device *device = user_data;
  gint mode;

  mode = device->status2 & 0x3;

  if (mode != CUBE_MODE_AUTO && mode != CUBE_MODE_MANUAL) {
    return;
  }

  if (mode == state) {
    cube_set_device_temperature (cube_get_active (), device, state ? CUBE_MODE_AUTO : CUBE_MODE_MANUAL, device->config->comfort_temp, 0, 0);
  }
}

void temperature_apply_clicked(GtkWidget *widget,
                               gpointer   user_data)
{
  GtkWidget *spin_button = user_data;
  struct cube_device *device = g_object_get_data (G_OBJECT (spin_button), "device");

  g_print ("%f", gtk_spin_button_get_value (GTK_SPIN_BUTTON (spin_button)));
  cube_set_device_temperature (cube_get_active (), device, cube_device_get_mode (device), gtk_spin_button_get_value (GTK_SPIN_BUTTON (spin_button)), 0, 0);
}

gboolean temperature_button_press_event(GtkWidget *widget,
                                        GdkEvent  *event,
                                        gpointer   user_data)
{
  GtkWidget *popover;
  GtkWidget *box;
  GtkWidget *spin_button;
  GtkWidget *button;
  struct cube_device *device = user_data;

  popover = gtk_popover_new (widget);

  box = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 12);
  gtk_widget_set_margin_top (box, 18);
  gtk_widget_set_margin_bottom (box, 18);
  gtk_widget_set_margin_start (box, 18);
  gtk_widget_set_margin_end (box, 18);

  spin_button = gtk_spin_button_new_with_range (4.5, 30.5, 0.5);
  gtk_spin_button_set_value (GTK_SPIN_BUTTON (spin_button), device->set_temp);
  g_object_set_data (G_OBJECT (spin_button), "device", device);
  gtk_box_pack_start (GTK_BOX (box), spin_button, TRUE, TRUE, 0);

  button = gtk_button_new_with_label (_("Apply"));
  g_signal_connect (button, "clicked", G_CALLBACK (temperature_apply_clicked), spin_button);
  gtk_box_pack_start (GTK_BOX (box), button, TRUE, TRUE, 0);

  gtk_container_add (GTK_CONTAINER (popover), box);

  gtk_widget_show_all (box);

  gtk_popover_popup (GTK_POPOVER (popover));
  return TRUE;
}

gboolean max_rebuild_ui_idle(gpointer user_data)
{
  GtkWidget *frame;
  GList *list;
  struct cube *cube = user_data;

  g_mutex_lock (&max_ui_mutex);

  gtk_container_foreach (GTK_CONTAINER (max_room_flowbox), flowbox_delete, NULL);

  for (list = cube->rooms; list != NULL; list = list->next) {
    struct cube_room *room = list->data;
    struct cube_device *device = cube_room_get_device (room, CUBE_DEVICE_TYPE_HEATING);
    struct room_ui *ui = g_malloc0 (sizeof (struct room_ui));
    GtkWidget *grid = gtk_grid_new ();
    GtkWidget *frame_label;
    GtkWidget *event_box;
    GtkWidget *label;
    GtkWidget *box;
    gchar *tmp = NULL;
    gint i = 0;

    if (!device) {
      device = cube_room_get_device (room, CUBE_DEVICE_TYPE_WALL);
    }

    gtk_grid_set_row_spacing (GTK_GRID (grid), 6);
    gtk_grid_set_column_spacing (GTK_GRID (grid), 12);
    gtk_widget_set_margin_start (grid, 6);
    gtk_widget_set_margin_end (grid, 6);
    gtk_widget_set_margin_top (grid, 6);
    gtk_widget_set_margin_bottom (grid, 6);

    frame = gtk_frame_new (room->name->str);
    gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_ETCHED_OUT);
    gtk_widget_set_hexpand (frame, TRUE);
    gtk_widget_set_vexpand (frame, TRUE);
    gtk_widget_set_valign (frame, GTK_ALIGN_BASELINE);

    frame_label = gtk_frame_get_label_widget (GTK_FRAME (frame));
    ui->name = frame_label;
    tmp = g_strdup_printf ("<b>%s</b>", room->name->str);
    gtk_label_set_markup (GTK_LABEL (frame_label), tmp);
    g_free (tmp);

    box = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 6);
    gtk_grid_attach (GTK_GRID (grid), box, 0, i++, 3, 1);

    label = gtk_label_new ("");
    gtk_widget_set_hexpand (label, TRUE);
    gtk_widget_set_vexpand (label, TRUE);

    GtkWidget *down = gtk_button_new_with_label ("-");
    gtk_button_set_relief (GTK_BUTTON (down), GTK_RELIEF_NONE);
    gtk_box_pack_start (GTK_BOX (box), down, FALSE, FALSE, 6);

    if (device) {
      g_signal_connect (down, "clicked", G_CALLBACK (max_decrease_temp), device);
      tmp = g_strdup_printf ("<big>%.1f°C</big>", device->set_temp);
    } else {
      gtk_widget_set_sensitive (down, FALSE);
      tmp = g_strdup_printf ("<big>---</big>");
    }

    gtk_label_set_markup (GTK_LABEL (label), tmp);
    ui->temperature = label;
    g_free (tmp);

    event_box = gtk_event_box_new ();
    gtk_container_add (GTK_CONTAINER (event_box), label);

    g_signal_connect (event_box, "button-press-event", G_CALLBACK (temperature_button_press_event), device);
    gtk_box_pack_start (GTK_BOX (box), event_box, TRUE, TRUE, 6);

    GtkWidget *up = gtk_button_new_with_label ("+");
    gtk_button_set_relief (GTK_BUTTON (up), GTK_RELIEF_NONE);
    gtk_box_pack_start (GTK_BOX (box), up, FALSE, FALSE, 6);

    if (device) {
      g_signal_connect (up, "clicked", G_CALLBACK (max_increase_temp), device);
    } else {
      gtk_widget_set_sensitive (up, FALSE);
    }

    label = gtk_label_new (_("Auto Mode:"));
    gtk_grid_attach (GTK_GRID (grid), label, 0, i, 1, 1);

    GtkWidget *mode_switch = gtk_switch_new ();
    ui->mode = mode_switch;
    if (device) {
      g_signal_connect (mode_switch, "state-set", G_CALLBACK (max_mode_switch), device);
    } else {
      gtk_widget_set_sensitive (mode_switch, FALSE);
    }
    gtk_grid_attach (GTK_GRID (grid), mode_switch, 1, i++, 1, 1);

    ui->note = gtk_label_new ("");
    gtk_widget_set_halign (mode_switch, GTK_ALIGN_CENTER);
    gtk_grid_attach (GTK_GRID (grid), ui->note, 0, i, 2, 5);

    GMenu *menu = g_menu_new ();
    GMenuItem *item;
    GMenu *submenu;

    if (device && device->type != CUBE_DEVICE_TYPE_SHUTTER) {
      item = g_menu_item_new (_("Week program"), "app.program");
      g_menu_item_set_action_and_target (G_MENU_ITEM (item), "app.program", "i", device->rf_address);
      g_menu_append_item (menu, item);

      item = g_menu_item_new (_("Temperature"), "app.temperature");
      g_menu_item_set_action_and_target (G_MENU_ITEM (item), "app.temperature", "i", device->rf_address);
      g_menu_append_item (menu, item);

      submenu = g_menu_new ();
      g_menu_append_submenu (menu, _("Device Settings"), G_MENU_MODEL (submenu));

      if (device->type != CUBE_DEVICE_TYPE_WALL) {
        item = g_menu_item_new (_("Boost & Decalcification"), "app.boost");
        g_menu_item_set_action_and_target (G_MENU_ITEM (item), "app.boost", "i", device->rf_address);
        g_menu_append_item (submenu, item);
      } else {
        item = g_menu_item_new (_("Temperature Display Mode"), "app.display");
        g_menu_item_set_action_and_target (G_MENU_ITEM (item), "app.display", "i", device->rf_address);
        g_menu_append_item (submenu, item);
      }

      item = g_menu_item_new (_("Temperature Offsets"), "app.offset");
      g_menu_item_set_action_and_target (G_MENU_ITEM (item), "app.offset", "i", device->rf_address);
      g_menu_append_item (submenu, item);
    }

    submenu = g_menu_new ();
    g_menu_append_submenu (menu, _("Structure"), G_MENU_MODEL (submenu));

    item = g_menu_item_new (_("Rename room"), "app.rename_room");
    g_menu_item_set_action_and_target (G_MENU_ITEM (item), "app.rename_room", "i", room->id);
    g_menu_append_item (submenu, item);

    item = g_menu_item_new (_("Remove room"), "app.remove_room");
    g_menu_item_set_action_and_target (G_MENU_ITEM (item), "app.remove_room", "i", room->id);
    g_menu_append_item (submenu, item);

    item = g_menu_item_new (_("Rename device"), "app.rename_device");
    g_menu_item_set_action_and_target (G_MENU_ITEM (item), "app.rename_device", "i", room->id);
    g_menu_append_item (submenu, item);

    if (g_list_length (room->devices)) {
      item = g_menu_item_new (_("Remove device"), "app.remove_device");
      g_menu_item_set_action_and_target (G_MENU_ITEM (item), "app.remove_device", "i", room->id);
      g_menu_append_item (submenu, item);
    }

    GtkWidget *settings = gtk_menu_button_new ();
    gtk_widget_set_hexpand (settings, FALSE);
    gtk_widget_set_halign (settings, GTK_ALIGN_END);
    gtk_menu_button_set_direction (GTK_MENU_BUTTON (settings), GTK_ARROW_NONE);
    gtk_menu_button_set_menu_model (GTK_MENU_BUTTON (settings), G_MENU_MODEL (menu));
    gtk_grid_attach (GTK_GRID (grid), settings, 2, i++, 1, 1);

    gtk_container_add (GTK_CONTAINER (frame), grid);

    gtk_flow_box_insert (GTK_FLOW_BOX (max_room_flowbox), frame, -1);

    gtk_widget_show_all (frame);

    room->priv = ui;
  }

  g_mutex_unlock (&max_ui_mutex);

  return G_SOURCE_REMOVE;
}

void max_rebuild_ui(CubeObject  *obj,
                    struct cube *cube,
                    gpointer     user_data)
{
  g_idle_add (max_rebuild_ui_idle, cube);
}

gboolean max_update_ui_idle(gpointer user_data)
{
  struct cube *cube = user_data;
  GList *list;

  g_mutex_lock (&max_ui_mutex);

  for (list = cube->rooms; list != NULL; list = list->next) {
    struct cube_room *room = list->data;
    struct cube_device *device = cube_room_get_device (room, CUBE_DEVICE_TYPE_HEATING);           /*cube_get_device_by_address(cube, room->rf_address); */
    struct room_ui *ui = room->priv;
    gchar *tmp = NULL;
    GList *devices;
    GString *note;

    if (!ui) {
      continue;
    }

    note = g_string_new ("");

    if (!device) {
      device = cube_room_get_device (room, CUBE_DEVICE_TYPE_WALL);
    }

    if (ui->name) {
      if (device && device->current_temp) {
        tmp = g_strdup_printf ("<b>%s</b> (%.1f°C)", room->name->str, device->current_temp);
      } else {
        tmp = g_strdup_printf ("<b>%s</b>", room->name->str);
      }
      gtk_label_set_markup (GTK_LABEL (ui->name), tmp);
      g_free (tmp);
    }

    if (!device) {
      continue;
    }

    /* Check status of devices attached to this room */
    for (devices = cube->devices; devices != NULL; devices = devices->next) {
      struct cube_device *dev = devices->data;

      if (dev->room_id != room->id) {
        continue;
      }

      if (dev->status1 & 0x8) {
        g_string_append_printf (note, _("Device error (%s)\n"), dev->name->str);
      }

      if ((dev->type == CUBE_DEVICE_TYPE_SHUTTER) && (cube_device_get_mode (dev) == 0x02)) {
        g_string_append_printf (note, _("Window open (%s)\n"), dev->name->str);
      }

      if (cube_device_is_battery_low (dev)) {
        g_string_append_printf (note, _("Battery low (%s)\n"), dev->name->str);
      }
    }


    if (ui->temperature && ui->mode) {
      tmp = g_strdup_printf ("<big>%.1f°C</big>", device->set_temp);
      gtk_label_set_markup (GTK_LABEL (ui->temperature), tmp);
      g_free (tmp);

      if (cube_device_get_mode (device) == CUBE_MODE_BOOST) {
        gtk_widget_set_sensitive (ui->mode, FALSE);
      } else {
        gtk_widget_set_sensitive (ui->mode, TRUE);
      }

      gtk_switch_set_state (GTK_SWITCH (ui->mode), !cube_device_get_mode (device));
    }

    if (ui->note) {
      if (device && cube_device_get_mode (device) == CUBE_MODE_VACATION) {
        gint day, month, year, hour, min;

        cube_get_date (device->date_until, &day, &month, &year);
        cube_get_time (device->time_until, &hour, &min);
        g_string_append_printf (note, _("Till %2.2d.%2.2d.%4.4d %2.2d:%2.2d\n"), day, month, year, hour, min);
      }

      if (note->len) {
        note = g_string_erase (note, note->len - 1, -1);
        note = g_string_prepend (note, _("<b>Note:</b>\n"));
      }

      gtk_label_set_markup (GTK_LABEL (ui->note), note->str);
    }

    g_string_free (note, TRUE);
  }

  g_mutex_unlock (&max_ui_mutex);

  return G_SOURCE_REMOVE;
}

void max_update_ui(CubeObject  *obj,
                   struct cube *cube,
                   gpointer     user_data)
{
  g_idle_add (max_update_ui_idle, cube);
}

/**
 * \brief Start cube processing
 * Start cube transmission thread
 * \param cube cube
 */
void max_start(struct cube *cube)
{
  g_mutex_init (&max_ui_mutex);

  /* Start transmission thread */
  cube_start_thread (cube);
}

void max_quit(GtkWidget *object,
              gpointer   user_data)
{
  struct cube *cube = user_data;

  cube_shutdown (cube);
}

void max_rename_room_response_cb(GtkDialog *dialog,
                                 gint       response,
                                 gpointer   user_data)
{
  if (response == GTK_RESPONSE_OK) {
    GtkWidget *name = user_data;
    struct cube *cube = g_object_get_data (G_OBJECT (name), "cube");
    struct cube_room *room = g_object_get_data (G_OBJECT (name), "room");

    cube_rename_room (cube, room, gtk_entry_get_text (GTK_ENTRY (name)));
  }

  gtk_widget_destroy (GTK_WIDGET (dialog));
}

void max_rename_room(GSimpleAction *action,
                     GVariant      *parameter,
                     gpointer       user_data)
{
  gint id = g_variant_get_int32 (parameter);
  struct cube *cube = cube_get_active ();
  struct cube_room *room = cube_get_room_by_id (cube, id);
  GtkWidget *dialog;
  GtkWidget *content_area;
  GtkWidget *name;
  GtkWidget *grid;
  GtkWidget *label;

  dialog = g_object_new (GTK_TYPE_DIALOG, "use-header-bar", TRUE, NULL);

  gtk_window_set_title (GTK_WINDOW (dialog), _("Room"));
  gtk_window_set_modal (GTK_WINDOW (dialog), TRUE);
  gtk_window_set_transient_for (GTK_WINDOW (dialog), GTK_WINDOW (max_main_window));
  gtk_dialog_add_button (GTK_DIALOG (dialog), _("Cancel"), GTK_RESPONSE_CANCEL);
  GtkWidget *remove = gtk_dialog_add_button (GTK_DIALOG (dialog), _("Rename"), GTK_RESPONSE_OK);
  gtk_style_context_add_class (gtk_widget_get_style_context (remove), GTK_STYLE_CLASS_SUGGESTED_ACTION);

  content_area = gtk_dialog_get_content_area (GTK_DIALOG (dialog));

  grid = gtk_grid_new ();
  gtk_grid_set_row_spacing (GTK_GRID (grid), 6);
  gtk_grid_set_column_spacing (GTK_GRID (grid), 12);

  gtk_widget_set_margin_start (grid, 18);
  gtk_widget_set_margin_end (grid, 18);
  gtk_widget_set_margin_top (grid, 18);
  gtk_widget_set_margin_bottom (grid, 18);

  label = ui_label_new (_("New name:"));
  gtk_grid_attach (GTK_GRID (grid), label, 0, 0, 1, 1);

  name = gtk_entry_new ();
  g_object_set_data (G_OBJECT (name), "cube", cube);
  g_object_set_data (G_OBJECT (name), "room", room);
  gtk_entry_set_max_length (GTK_ENTRY (name), 31);
  gtk_widget_set_hexpand (name, TRUE);
  gtk_entry_set_text (GTK_ENTRY (name), room->name->str);
  gtk_grid_attach (GTK_GRID (grid), name, 1, 0, 1, 1);

  gtk_container_add (GTK_CONTAINER (content_area), grid);
  g_signal_connect (dialog, "response", G_CALLBACK (max_rename_room_response_cb), name);

  gtk_widget_show_all (dialog);
}

void max_remove_room_response_cb(GtkDialog *dialog,
                                 gint       response,
                                 gpointer   user_data)
{
  if (response == GTK_RESPONSE_OK) {
    struct cube *cube = g_object_get_data (G_OBJECT (dialog), "cube");
    struct cube_room *room = g_object_get_data (G_OBJECT (dialog), "room");

    cube_remove_room (room->cube, room);

    max_rebuild_ui (NULL, cube, NULL);
  }

  gtk_widget_destroy (GTK_WIDGET (dialog));
}

void max_remove_room(GSimpleAction *action,
                     GVariant      *parameter,
                     gpointer       user_data)
{
  gint id = g_variant_get_int32 (parameter);
  struct cube *cube = cube_get_active ();
  struct cube_room *room = cube_get_room_by_id (cube, id);
  GtkWidget *dialog;

  gchar *tmp;
  dialog = g_object_new (GTK_TYPE_DIALOG, "use-header-bar", TRUE, NULL);

  g_object_set_data (G_OBJECT (dialog), "cube", cube);
  g_object_set_data (G_OBJECT (dialog), "room", room);

  gtk_window_set_title (GTK_WINDOW (dialog), _("Room"));
  gtk_window_set_modal (GTK_WINDOW (dialog), TRUE);
  gtk_window_set_transient_for (GTK_WINDOW (dialog), GTK_WINDOW (max_main_window));
  gtk_dialog_add_button (GTK_DIALOG (dialog), _("Cancel"), GTK_RESPONSE_CANCEL);
  GtkWidget *remove = gtk_dialog_add_button (GTK_DIALOG (dialog), _("Remove"), GTK_RESPONSE_OK);
  gtk_style_context_add_class (gtk_widget_get_style_context (remove), GTK_STYLE_CLASS_DESTRUCTIVE_ACTION);
  GtkWidget *content = gtk_dialog_get_content_area (GTK_DIALOG (dialog));

  tmp = g_strdup_printf (_("Do you really want to remove '%s' with all its devices?"), room->name->str);
  GtkWidget *label = gtk_label_new (tmp);
  gtk_container_add (GTK_CONTAINER (content), label);

  gtk_widget_set_margin_start (label, 18);
  gtk_widget_set_margin_end (label, 18);
  gtk_widget_set_margin_top (label, 18);
  gtk_widget_set_margin_bottom (label, 18);
  gtk_widget_show_all (content);
  g_free (tmp);

  g_signal_connect (dialog, "response", G_CALLBACK (max_remove_room_response_cb), NULL);

  gtk_widget_show (dialog);
}

void max_rename_device(GSimpleAction *action,
                       GVariant      *parameter,
                       gpointer       user_data)
{
  gint id = g_variant_get_int32 (parameter);
  struct cube *cube = cube_get_active ();
  struct cube_room *room = cube_get_room_by_id (cube, id);
  GtkWidget *dialog;
  GtkWidget *content_area = NULL;
  GtkWidget *grid;
  GList *list;
  GList *names = NULL;
  gint ret;
  gint i = 0;

  dialog = g_object_new (GTK_TYPE_DIALOG, "use-header-bar", TRUE, NULL);

  g_object_set_data (G_OBJECT (dialog), "cube", cube);
  g_object_set_data (G_OBJECT (dialog), "room", room);

  gtk_window_set_title (GTK_WINDOW (dialog), _("Device"));
  gtk_window_set_modal (GTK_WINDOW (dialog), TRUE);
  gtk_window_set_transient_for (GTK_WINDOW (dialog), GTK_WINDOW (max_main_window));
  gtk_dialog_add_button (GTK_DIALOG (dialog), _("Cancel"), GTK_RESPONSE_CANCEL);
  GtkWidget *remove = gtk_dialog_add_button (GTK_DIALOG (dialog), _("Rename"), GTK_RESPONSE_OK);
  gtk_style_context_add_class (gtk_widget_get_style_context (remove), GTK_STYLE_CLASS_SUGGESTED_ACTION);
  content_area = gtk_dialog_get_content_area (GTK_DIALOG (dialog));

  grid = gtk_grid_new ();
  gtk_container_add (GTK_CONTAINER (content_area), grid);
  gtk_grid_set_row_spacing (GTK_GRID (grid), 6);
  gtk_grid_set_column_spacing (GTK_GRID (grid), 12);

  gtk_widget_set_margin_start (grid, 18);
  gtk_widget_set_margin_end (grid, 18);
  gtk_widget_set_margin_top (grid, 18);
  gtk_widget_set_margin_bottom (grid, 18);

  for (list = room->devices; list != NULL; list = list->next) {
    GtkWidget *name;
    GtkWidget *label;
    struct cube_device *device = list->data;

    label = gtk_label_new (/*_("Name:")*/ device->serial_number->str);
    gtk_grid_attach (GTK_GRID (grid), label, 0, i, 1, 1);

    name = gtk_entry_new ();
    gtk_grid_attach (GTK_GRID (grid), name, 1, i, 1, 1);
    gtk_entry_set_max_length (GTK_ENTRY (name), 31);
    gtk_entry_set_text (GTK_ENTRY (name), device->name->str);

    g_object_set_data (G_OBJECT (name), "device", device);
    names = g_list_append (names, name);
    i++;
  }

  gtk_widget_show_all (content_area);
  ret = gtk_dialog_run (GTK_DIALOG (dialog));

  if (ret != GTK_RESPONSE_OK) {
    gtk_widget_destroy (dialog);
    return;
  }

  for (list = names; list != NULL; list = list->next) {
    GtkWidget *widget = list->data;
    struct cube_device *device = g_object_get_data (G_OBJECT (widget), "device");

    if (strcmp (gtk_entry_get_text (GTK_ENTRY (widget)), device->name->str)) {
      g_print ("Name changed: %s\n", device->name->str);
      cube_rename_device (cube_get_active (), device, gtk_entry_get_text (GTK_ENTRY (widget)));
    }
  }

  gtk_widget_destroy (dialog);
}

void max_remove_device(GSimpleAction *action,
                       GVariant      *parameter,
                       gpointer       user_data)
{
  gint id = g_variant_get_int32 (parameter);
  struct cube *cube = cube_get_active ();
  struct cube_room *room = cube_get_room_by_id (cube, id);
  GtkWidget *dialog;
  GtkWidget *content_area;
  gint ret;
  GtkWidget *grid;
  GList *list;
  gint i = 0;
  GList *buttons = NULL;

  /*dialog = gtk_dialog_new(); */
  dialog = g_object_new (GTK_TYPE_DIALOG, "use-header-bar", TRUE, NULL);

  g_object_set_data (G_OBJECT (dialog), "cube", cube);
  g_object_set_data (G_OBJECT (dialog), "room", room);

  gtk_window_set_title (GTK_WINDOW (dialog), _("Devices"));
  gtk_window_set_modal (GTK_WINDOW (dialog), TRUE);
  gtk_window_set_transient_for (GTK_WINDOW (dialog), GTK_WINDOW (max_main_window));
  gtk_dialog_add_button (GTK_DIALOG (dialog), _("Cancel"), GTK_RESPONSE_CANCEL);
  GtkWidget *remove = gtk_dialog_add_button (GTK_DIALOG (dialog), _("Remove"), GTK_RESPONSE_OK);

  gtk_style_context_add_class (gtk_widget_get_style_context (remove), GTK_STYLE_CLASS_DESTRUCTIVE_ACTION);

  content_area = gtk_dialog_get_content_area (GTK_DIALOG (dialog));

  grid = gtk_grid_new ();
  gtk_grid_set_row_spacing (GTK_GRID (grid), 6);
  gtk_grid_set_column_spacing (GTK_GRID (grid), 12);

  gtk_widget_set_margin_start (content_area, 18);
  gtk_widget_set_margin_end (content_area, 18);
  gtk_widget_set_margin_top (content_area, 18);
  gtk_widget_set_margin_bottom (content_area, 18);

  for (list = room->devices; list != NULL; list = list->next) {
    struct cube_device *device = list->data;
    GtkWidget *check_button = gtk_check_button_new_with_label (device->name->str);

    g_object_set_data (G_OBJECT (check_button), "device", device);
    buttons = g_list_append (buttons, check_button);
    gtk_grid_attach (GTK_GRID (grid), check_button, 0, i++, 1, 1);
  }

  gtk_container_add (GTK_CONTAINER (content_area), grid);
  gtk_widget_show_all (content_area);

  ret = gtk_dialog_run (GTK_DIALOG (dialog));

  if (ret != GTK_RESPONSE_OK) {
    gtk_widget_destroy (dialog);
    return;
  }

  for (list = buttons, i = 0; list != NULL; list = list->next, i++) {
    GtkWidget *widget = list->data;

    if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (widget))) {
      struct cube_device *device = g_object_get_data (G_OBJECT (widget), "device");

      if (device) {
        g_print ("Removing '%s'\n", device->name->str);
        cube_room_remove_device (room, device);
      }
    }
  }

  g_list_free (buttons);

  gtk_widget_destroy (dialog);

  max_rebuild_ui (NULL, cube, NULL);
}

void max_temperature_response_cb(GtkDialog *dialog,
                                 gint       response,
                                 gpointer   user_data)
{
  if (response == GTK_RESPONSE_OK) {
    struct cube_device *device = user_data;
    struct cube_room *room = cube_get_room_by_id (cube_get_active (), device->room_id);
    GtkWidget *eco_spin_button = g_object_get_data (G_OBJECT (dialog), "eco");
    GtkWidget *comfort_spin_button = g_object_get_data (G_OBJECT (dialog), "comfort");
    GtkWidget *window_open_temp_spin_button = g_object_get_data (G_OBJECT (dialog), "max_main_window");
    GtkWidget *max_set_point_temp_spin_button = g_object_get_data (G_OBJECT (dialog), "max");

    cube_room_set_base_config (
      room,
      gtk_spin_button_get_value (GTK_SPIN_BUTTON (eco_spin_button)),
      gtk_spin_button_get_value (GTK_SPIN_BUTTON (comfort_spin_button)),
      gtk_spin_button_get_value (GTK_SPIN_BUTTON (window_open_temp_spin_button)),
      gtk_spin_button_get_value (GTK_SPIN_BUTTON (max_set_point_temp_spin_button)));
  }

  gtk_widget_destroy (GTK_WIDGET (dialog));
}

static void max_temperature(GSimpleAction *action,
                            GVariant      *parameter,
                            gpointer       user_data)
{
  gint address = g_variant_get_int32 (parameter);
  struct cube *cube = cube_get_active ();
  struct cube_device *device = cube_get_device_by_address (cube, address);
  GtkWidget *dialog;
  GtkWidget *grid;
  GtkWidget *content_area;
  GtkWidget *label;
  GtkWidget *eco_spin_button;
  GtkWidget *comfort_spin_button;
  GtkWidget *window_open_temp_spin_button;
  GtkWidget *max_set_point_temp_spin_button;

  dialog = gtk_dialog_new_with_buttons (_("Temperatures"), GTK_WINDOW (max_main_window), GTK_DIALOG_MODAL | GTK_DIALOG_USE_HEADER_BAR, _("Cancel"), GTK_RESPONSE_CANCEL, _("Apply"), GTK_RESPONSE_OK, NULL);
  content_area = gtk_dialog_get_content_area (GTK_DIALOG (dialog));

  grid = gtk_grid_new ();
  gtk_widget_set_hexpand (grid, TRUE);

  gtk_grid_set_row_spacing (GTK_GRID (grid), 6);
  gtk_grid_set_column_spacing (GTK_GRID (grid), 12);
  gtk_widget_set_margin_start (grid, 18);
  gtk_widget_set_margin_end (grid, 18);
  gtk_widget_set_margin_top (grid, 18);
  gtk_widget_set_margin_bottom (grid, 18);

  gtk_container_add (GTK_CONTAINER (content_area), grid);

  label = ui_label_new (_("Eco temperature:"));
  gtk_grid_attach (GTK_GRID (grid), label, 0, 0, 1, 1);

  eco_spin_button = gtk_spin_button_new_with_range (4.5, 30.5, 0.5);
  gtk_spin_button_set_value (GTK_SPIN_BUTTON (eco_spin_button), device->config->eco_temp);
  gtk_grid_attach (GTK_GRID (grid), eco_spin_button, 1, 0, 1, 1);

  label = gtk_label_new ("°C");
  gtk_grid_attach (GTK_GRID (grid), label, 2, 0, 1, 1);

  label = ui_label_new (_("Comfort temperature:"));
  gtk_grid_attach (GTK_GRID (grid), label, 0, 1, 1, 1);

  comfort_spin_button = gtk_spin_button_new_with_range (4.5, 30.5, 0.5);
  gtk_spin_button_set_value (GTK_SPIN_BUTTON (comfort_spin_button), device->config->comfort_temp);
  gtk_grid_attach (GTK_GRID (grid), comfort_spin_button, 1, 1, 1, 1);

  label = gtk_label_new ("°C");
  gtk_grid_attach (GTK_GRID (grid), label, 2, 1, 1, 1);

  label = ui_label_new (_("Window open:"));
  gtk_grid_attach (GTK_GRID (grid), label, 0, 2, 1, 1);

  window_open_temp_spin_button = gtk_spin_button_new_with_range (4.5, 30.5, 0.5);
  gtk_spin_button_set_value (GTK_SPIN_BUTTON (window_open_temp_spin_button), device->config->window_open_temp);
  gtk_grid_attach (GTK_GRID (grid), window_open_temp_spin_button, 1, 2, 1, 1);

  label = gtk_label_new ("°C");
  gtk_grid_attach (GTK_GRID (grid), label, 2, 2, 1, 1);

  label = ui_label_new (_("Max. temperature:"));
  gtk_grid_attach (GTK_GRID (grid), label, 0, 3, 1, 1);

  max_set_point_temp_spin_button = gtk_spin_button_new_with_range (4.5, 30.5, 0.5);
  gtk_spin_button_set_value (GTK_SPIN_BUTTON (max_set_point_temp_spin_button), device->config->max_set_point_temp);
  gtk_grid_attach (GTK_GRID (grid), max_set_point_temp_spin_button, 1, 3, 1, 1);

  label = gtk_label_new ("°C");
  gtk_grid_attach (GTK_GRID (grid), label, 2, 3, 1, 1);

  gtk_widget_show_all (content_area);
  g_signal_connect (dialog, "response", G_CALLBACK (max_temperature_response_cb), device);

  g_object_set_data (G_OBJECT (dialog), "eco", eco_spin_button);
  g_object_set_data (G_OBJECT (dialog), "comfort", comfort_spin_button);
  g_object_set_data (G_OBJECT (dialog), "max_main_window", window_open_temp_spin_button);
  g_object_set_data (G_OBJECT (dialog), "max", max_set_point_temp_spin_button);
  gtk_widget_show (dialog);
}

static void max_internet(GSimpleAction *action,
                         GVariant      *parameter,
                         gpointer       user_data)
{
  struct cube *cube = cube_get_active ();
  GtkWidget *dialog;
  GtkWidget *grid;
  GtkWidget *content_area;
  GtkWidget *control_switch;
  GtkWidget *label;
  GtkWidget *user_entry;
  GtkWidget *password_entry;

  dialog = g_object_new (GTK_TYPE_DIALOG, "use-header-bar", TRUE, NULL);

  gtk_window_set_title (GTK_WINDOW (dialog), _("Internet control"));
  gtk_window_set_modal (GTK_WINDOW (dialog), TRUE);
  gtk_window_set_transient_for (GTK_WINDOW (dialog), GTK_WINDOW (max_main_window));
  gtk_dialog_add_button (GTK_DIALOG (dialog), _("Cancel"), GTK_RESPONSE_CANCEL);
  GtkWidget *apply = gtk_dialog_add_button (GTK_DIALOG (dialog), _("Apply"), GTK_RESPONSE_OK);
  gtk_style_context_add_class (gtk_widget_get_style_context (apply), GTK_STYLE_CLASS_SUGGESTED_ACTION);
  content_area = gtk_dialog_get_content_area (GTK_DIALOG (dialog));

  grid = gtk_grid_new ();
  gtk_widget_set_hexpand (grid, TRUE);

  gtk_grid_set_row_spacing (GTK_GRID (grid), 6);
  gtk_grid_set_column_spacing (GTK_GRID (grid), 12);
  gtk_widget_set_margin_start (grid, 18);
  gtk_widget_set_margin_end (grid, 18);
  gtk_widget_set_margin_top (grid, 18);
  gtk_widget_set_margin_bottom (grid, 18);

  gtk_container_add (GTK_CONTAINER (content_area), grid);

  label = ui_label_new (_("Remote control"));
  gtk_grid_attach (GTK_GRID (grid), label, 0, 0, 1, 1);

  control_switch = gtk_switch_new ();
  gtk_switch_set_active (GTK_SWITCH (control_switch), cube->internet_control);
  gtk_widget_set_halign (control_switch, GTK_ALIGN_START);
  gtk_grid_attach (GTK_GRID (grid), control_switch, 1, 0, 1, 1);

  label = ui_label_new (_("User"));
  gtk_grid_attach (GTK_GRID (grid), label, 0, 1, 1, 1);

  user_entry = gtk_entry_new ();
  gtk_entry_set_text (GTK_ENTRY (user_entry), cube->user ? cube->user : "");
  gtk_grid_attach (GTK_GRID (grid), user_entry, 1, 1, 1, 1);

  label = ui_label_new (_("Password"));
  gtk_grid_attach (GTK_GRID (grid), label, 0, 2, 1, 1);

  password_entry = gtk_entry_new ();
  gtk_entry_set_text (GTK_ENTRY (password_entry), "");
  gtk_entry_set_visibility (GTK_ENTRY (password_entry), FALSE);
  gtk_grid_attach (GTK_GRID (grid), password_entry, 1, 2, 1, 1);

  gtk_widget_show_all (content_area);

  gtk_dialog_run (GTK_DIALOG (dialog));
  gtk_widget_destroy (dialog);
}

static void max_account(GSimpleAction *action,
                        GVariant      *parameter,
                        gpointer       user_data)
{
  struct cube *cube = cube_get_active ();
  GtkWidget *dialog;
  GtkWidget *grid;
  GtkWidget *content_area;
  GtkWidget *label;
  gchar *tmp;

  dialog = gtk_dialog_new_with_buttons (_("Account"), GTK_WINDOW (max_main_window), GTK_DIALOG_MODAL | GTK_DIALOG_USE_HEADER_BAR, _("Close"), GTK_RESPONSE_OK, NULL);
  content_area = gtk_dialog_get_content_area (GTK_DIALOG (dialog));

  grid = gtk_grid_new ();
  gtk_widget_set_hexpand (grid, TRUE);

  gtk_grid_set_row_spacing (GTK_GRID (grid), 6);
  gtk_grid_set_column_spacing (GTK_GRID (grid), 12);
  gtk_widget_set_margin_start (grid, 18);
  gtk_widget_set_margin_end (grid, 18);
  gtk_widget_set_margin_top (grid, 18);
  gtk_widget_set_margin_bottom (grid, 18);

  gtk_container_add (GTK_CONTAINER (content_area), grid);

  tmp = g_strdup_printf (_("Your MAX! Account will remain active for %d days and expire on %s"), cube->remaining_days, cube->end_date ? cube->end_date : "?");
  label = gtk_label_new (tmp);
  g_free (tmp);
  gtk_grid_attach (GTK_GRID (grid), label, 0, 0, 1, 1);

  gtk_widget_show_all (content_area);

  gtk_dialog_run (GTK_DIALOG (dialog));
  gtk_widget_destroy (dialog);
}

static void max_boost(GSimpleAction *action,
                      GVariant      *parameter,
                      gpointer       user_data)
{
  gint address = g_variant_get_int32 (parameter);
  struct cube *cube = cube_get_active ();
  struct cube_device *device = cube_get_device_by_address (cube, address);
  struct cube_room *room = cube_get_room_by_id (cube, device->room_id);
  GtkWidget *dialog;
  GtkWidget *grid;
  GtkWidget *content_area;
  GtkWidget *label;
  GtkWidget *boost_duration_spin_button;
  GtkWidget *boost_valve_spin_button;
  gint ret;

  /*dialog = gtk_dialog_new_with_buttons(_("Boost & Decalcification"), GTK_WINDOW(max_main_window), GTK_DIALOG_MODAL | GTK_DIALOG_USE_HEADER_BAR, _("Cancel"), GTK_RESPONSE_CANCEL, _("Apply"), GTK_RESPONSE_OK, NULL); */
  /*content_area = gtk_dialog_get_content_area(GTK_DIALOG(dialog)); */
  dialog = g_object_new (GTK_TYPE_DIALOG, "use-header-bar", TRUE, NULL);

  gtk_window_set_modal (GTK_WINDOW (dialog), TRUE);
  gtk_window_set_transient_for (GTK_WINDOW (dialog), GTK_WINDOW (max_main_window));
  gtk_dialog_add_button (GTK_DIALOG (dialog), _("Cancel"), GTK_RESPONSE_CANCEL);
  gtk_window_set_title (GTK_WINDOW (dialog), _("Boost & Decalcification"));
  GtkWidget *apply = gtk_dialog_add_button (GTK_DIALOG (dialog), _("Apply"), GTK_RESPONSE_OK);
  gtk_style_context_add_class (gtk_widget_get_style_context (apply), GTK_STYLE_CLASS_SUGGESTED_ACTION);

  content_area = gtk_dialog_get_content_area (GTK_DIALOG (dialog));

  grid = gtk_grid_new ();
  gtk_grid_set_row_spacing (GTK_GRID (grid), 6);
  gtk_grid_set_column_spacing (GTK_GRID (grid), 12);

  gtk_widget_set_margin_start (grid, 18);
  gtk_widget_set_margin_end (grid, 18);
  gtk_widget_set_margin_top (grid, 18);
  gtk_widget_set_margin_bottom (grid, 18);

  gtk_container_add (GTK_CONTAINER (content_area), grid);

  label = ui_label_new (_("Boost duration:"));
  gtk_grid_attach (GTK_GRID (grid), label, 0, 0, 1, 1);

  gint duration = device->config->boost_duration_val >> 5;
  gint value = device->config->boost_duration_val & 0x1F;

  boost_duration_spin_button = gtk_spin_button_new_with_range (5, 60, 5);
  gtk_spin_button_set_value (GTK_SPIN_BUTTON (boost_duration_spin_button), duration == 7 ? 30 : duration * 5);
  gtk_grid_attach (GTK_GRID (grid), boost_duration_spin_button, 1, 0, 1, 1);

  label = gtk_label_new (_("min"));
  gtk_grid_attach (GTK_GRID (grid), label, 2, 0, 1, 1);

  label = ui_label_new (_("Boost valve:"));
  gtk_grid_attach (GTK_GRID (grid), label, 0, 1, 1, 1);

  boost_valve_spin_button = gtk_spin_button_new_with_range (5, 100, 5);
  gtk_spin_button_set_value (GTK_SPIN_BUTTON (boost_valve_spin_button), value * 5);
  gtk_grid_attach (GTK_GRID (grid), boost_valve_spin_button, 1, 1, 1, 1);

  label = gtk_label_new ("%");
  gtk_grid_attach (GTK_GRID (grid), label, 2, 1, 1, 1);

  label = ui_label_new (_("Decalcification:"));
  gtk_grid_attach (GTK_GRID (grid), label, 0, 2, 1, 1);

  GtkWidget *day_box = gtk_combo_box_text_new ();
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (day_box), _("Sat"));
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (day_box), _("Sun"));
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (day_box), _("Mon"));
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (day_box), _("Tue"));
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (day_box), _("Wed"));
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (day_box), _("Thu"));
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (day_box), _("Fri"));
  gtk_combo_box_set_active (GTK_COMBO_BOX (day_box), device->config->decalcification >> 5);
  gtk_grid_attach (GTK_GRID (grid), day_box, 1, 2, 1, 1);

  GtkWidget *time_box = gtk_combo_box_text_new ();
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (time_box), "00:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (time_box), "01:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (time_box), "02:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (time_box), "03:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (time_box), "04:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (time_box), "05:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (time_box), "06:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (time_box), "07:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (time_box), "08:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (time_box), "09:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (time_box), "10:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (time_box), "11:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (time_box), "12:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (time_box), "13:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (time_box), "14:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (time_box), "15:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (time_box), "16:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (time_box), "17:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (time_box), "18:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (time_box), "19:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (time_box), "20:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (time_box), "21:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (time_box), "22:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (time_box), "23:00");
  gtk_combo_box_set_active (GTK_COMBO_BOX (time_box), device->config->decalcification & 0x1F);
  gtk_grid_attach (GTK_GRID (grid), time_box, 2, 2, 1, 1);

  gtk_widget_show_all (content_area);
  ret = gtk_dialog_run (GTK_DIALOG (dialog));

  if (ret != GTK_RESPONSE_OK) {
    gtk_widget_destroy (dialog);
    return;
  }

  duration = gtk_spin_button_get_value (GTK_SPIN_BUTTON (boost_duration_spin_button));
  gint val = gtk_spin_button_get_value (GTK_SPIN_BUTTON (boost_valve_spin_button));

  cube_room_config_valve (room, duration, val, gtk_combo_box_get_active (GTK_COMBO_BOX (day_box)), gtk_combo_box_get_active (GTK_COMBO_BOX (time_box)), 0, 0);

  gtk_widget_destroy (dialog);
}

void stack_delete(GtkWidget *widget,
                  gpointer   user_data)
{
  gtk_widget_destroy (widget);
}

GtkWidget *create_week_day (gint id);

void rebuild_week_ui(void)
{
  GtkWidget *week_day;

  gtk_container_foreach (GTK_CONTAINER (stack), stack_delete, NULL);

  week_day = create_week_day (2);
  gtk_stack_add_titled (GTK_STACK (stack), week_day, _("Mon"), _("Mon"));
  week_day = create_week_day (3);
  gtk_stack_add_titled (GTK_STACK (stack), week_day, _("Tue"), _("Tue"));
  week_day = create_week_day (4);
  gtk_stack_add_titled (GTK_STACK (stack), week_day, _("Wed"), _("Wed"));
  week_day = create_week_day (5);
  gtk_stack_add_titled (GTK_STACK (stack), week_day, _("Thu"), _("Thu"));
  week_day = create_week_day (6);
  gtk_stack_add_titled (GTK_STACK (stack), week_day, _("Fri"), _("Fri"));
  week_day = create_week_day (0);
  gtk_stack_add_titled (GTK_STACK (stack), week_day, _("Sat"), _("Sat"));
  week_day = create_week_day (1);
  gtk_stack_add_titled (GTK_STACK (stack), week_day, _("Sun"), _("Sun"));
}

void week_day_edit_cb(GtkWidget *button,
                      gpointer   user_data)
{
  GtkWidget *dialog;
  GtkWidget *container;
  GtkWidget *temperature_spin_button;
  GtkWidget *hour_spin_button;
  GtkWidget *min_spin_button;
  GtkWidget *grid;
  GtkWidget *label;
  struct cube_day_entry *entry = user_data;
  gint ret;
  gint hour;
  gint min;

  hour = entry->min * 5 / 60;
  min = entry->min * 5 % 60;

  dialog = gtk_dialog_new_with_buttons (_("Edit entry"), GTK_WINDOW (max_main_window), GTK_DIALOG_MODAL, _("Cancel"), GTK_RESPONSE_CANCEL, _("Apply"), GTK_RESPONSE_OK, NULL);
  container = gtk_dialog_get_content_area (GTK_DIALOG (dialog));

  grid = gtk_grid_new ();
  gtk_grid_set_row_spacing (GTK_GRID (grid), 6);
  gtk_grid_set_column_spacing (GTK_GRID (grid), 12);

  gtk_widget_set_margin_start (grid, 18);
  gtk_widget_set_margin_end (grid, 18);
  gtk_widget_set_margin_top (grid, 18);
  gtk_widget_set_margin_bottom (grid, 18);

  gtk_container_add (GTK_CONTAINER (container), grid);

  label = gtk_label_new (_("Until "));
  gtk_grid_attach (GTK_GRID (grid), label, 0, 0, 1, 1);

  hour_spin_button = gtk_spin_button_new_with_range (0, 23, 1);
  gtk_spin_button_set_value (GTK_SPIN_BUTTON (hour_spin_button), hour);
  gtk_grid_attach (GTK_GRID (grid), hour_spin_button, 1, 0, 1, 1);

  min_spin_button = gtk_spin_button_new_with_range (0, 55, 5);
  gtk_spin_button_set_value (GTK_SPIN_BUTTON (min_spin_button), min);
  gtk_grid_attach (GTK_GRID (grid), min_spin_button, 2, 0, 1, 1);

  label = gtk_label_new (":");
  gtk_grid_attach (GTK_GRID (grid), label, 3, 0, 1, 1);

  temperature_spin_button = gtk_spin_button_new_with_range (4.5, 30.5, 0.5);
  gtk_spin_button_set_value (GTK_SPIN_BUTTON (temperature_spin_button), entry->deg);
  gtk_grid_attach (GTK_GRID (grid), temperature_spin_button, 4, 0, 1, 1);
  gtk_widget_show_all (container);

  ret = gtk_dialog_run (GTK_DIALOG (dialog));

  if (ret != GTK_RESPONSE_OK) {
    gtk_widget_destroy (dialog);
    return;
  }

  entry->deg = gtk_spin_button_get_value (GTK_SPIN_BUTTON (temperature_spin_button));
  hour = gtk_spin_button_get_value (GTK_SPIN_BUTTON (hour_spin_button));
  min = gtk_spin_button_get_value (GTK_SPIN_BUTTON (min_spin_button));

  entry->min = (hour * 60 + min) / 5;

  gtk_widget_destroy (dialog);

  rebuild_week_ui ();
}

void week_day_remove_cb(GtkWidget *button,
                        gpointer   user_data)
{
  gint id = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (button), "list"));
  GList *list = week_days[id];
  GList *pos;
  GList *prev;
  GList *next;
  struct cube_day_entry *entry;
  struct cube_day_entry *entry_prev;
  /*struct cube_day_entry *entry_next; */

  pos = g_list_find (list, user_data);
  if (!pos) {
    g_print ("pos not found, abort\n");
    return;
  }

  entry = pos->data;
  prev = pos->prev;
  next = pos->next;

  if (!prev) {
    /* First element */
    entry->min = 288;
    list = g_list_remove (list, entry);
    week_days[id] = g_list_append (list, entry);

    g_print ("first\n");
  } else if (!next) {
    /* Last element */
    entry_prev = prev->data;

    entry_prev->min = entry->min;
    g_print ("last\n");
  } else {
    /* In the middle */
    entry_prev = prev->data;
    entry_prev->min = entry->min;

    list = g_list_remove (list, entry);
    week_days[id] = g_list_append (list, entry);
  }

  rebuild_week_ui ();
}

void week_day_add_cb(GtkWidget *button,
                     gpointer   user_data)
{
  gint id = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (button), "list"));
  GList *list = week_days[id];
  GList *pos;
  GList *prev;
  GList *next;
  struct cube_day_entry *entry;
  struct cube_day_entry *entry_prev;
  struct cube_day_entry *entry_next;

  pos = g_list_find (list, user_data);
  if (!pos) {
    g_print ("pos not found, abort\n");
    return;
  }

  entry = pos->data;
  prev = pos->prev;
  /*entry_prev = prev->data; */
  next = pos->next;
  entry_next = next->data;

  if (!prev) {
    /* First element */
    GList *last = g_list_last (list);

    list = g_list_remove_link (list, last);
    entry = last->data;
    entry->deg = 17;
    entry->min = 1;
    week_days[id] = g_list_concat (last, list);

    g_print ("first\n");
  } else if (entry_next->min != 288) {
    /* In the middle */
    GList *last = g_list_last (list);
    prev = last->prev;
    entry_prev = prev->data;

    list = g_list_remove_link (list, last);
    entry = last->data;
    entry->deg = 17;
    entry->min = entry_prev->min + 1;
    week_days[id] = g_list_concat (last, list);

    g_print ("middle\n");
  } else {
    /* Last element */
    g_print ("middle\n");
    entry->min = 285;

    entry_next = next->data;
    entry_next->min = 288;
  }

  rebuild_week_ui ();
}

GtkWidget *create_week_day(gint id)
{
  GtkWidget *grid;
  gint i;
  gint used = 0;
  GList *list = week_days[id];
  GtkWidget *box;

  grid = gtk_grid_new ();
  gtk_widget_set_hexpand (grid, TRUE);
  gtk_grid_set_row_spacing (GTK_GRID (grid), 5);
  gtk_grid_set_column_spacing (GTK_GRID (grid), 5);

  for (i = 0; i < 13; i++) {
    struct cube_day_entry *entry = g_list_nth_data (list, i);

    /*g_print("min %d\n", entry->min); */
    if (entry->min < 288) {
      used++;
    } else {
      break;
    }
  }

  /*g_print("used: %d\n", used); */

  for (i = 0; i < 13; i++) {
    struct cube_day_entry *entry = g_list_nth_data (list, i);
    struct cube_day_entry *prev = i != 0 ? g_list_nth_data (list, i - 1) : NULL;
    gint hour = entry->min * 5 / 60;
    gint min = entry->min * 5 % 60;
    gint prev_hour = prev ? prev->min * 5 / 60 : 0;
    gint prev_min = prev ? prev->min * 5 % 60 : 0;

    if (prev_hour == 24) {
      break;
    }

    gchar *tmp;
    tmp = g_strdup_printf (_("From %2.2d:%2.2d to %2.2d:%2.2d: %.1f C"),
                           prev_hour,
                           prev_min,
                           hour,
                           min,
                           entry->deg);
    GtkWidget *label = gtk_label_new (tmp);
    gtk_widget_set_hexpand (label, TRUE);
    gtk_widget_set_halign (label, GTK_ALIGN_CENTER);
    g_free (tmp);
    gtk_grid_attach (GTK_GRID (grid), label, 0, i, 1, 1);

    box = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 0);
    gtk_style_context_add_class (gtk_widget_get_style_context (box), GTK_STYLE_CLASS_RAISED);
    gtk_style_context_add_class (gtk_widget_get_style_context (box), GTK_STYLE_CLASS_LINKED);
    gtk_grid_attach (GTK_GRID (grid), box, 1, i, 1, 1);

    GtkWidget *remove = gtk_button_new_from_icon_name ("list-remove-symbolic", GTK_ICON_SIZE_BUTTON);
    g_object_set_data (G_OBJECT (remove), "list", GINT_TO_POINTER (id));
    g_signal_connect (G_OBJECT (remove), "clicked", G_CALLBACK (week_day_remove_cb), entry);
    /*gtk_grid_attach(GTK_GRID(grid), remove, 1, i, 1, 1); */
    gtk_box_pack_start (GTK_BOX (box), remove, FALSE, TRUE, 0);

    gtk_widget_set_sensitive (remove, used != 0);

    GtkWidget *edit = gtk_button_new_from_icon_name ("preferences-system-symbolic", GTK_ICON_SIZE_BUTTON);
    g_signal_connect (G_OBJECT (edit), "clicked", G_CALLBACK (week_day_edit_cb), entry);
    /*gtk_grid_attach(GTK_GRID(grid), edit, 2, i, 1, 1); */
    gtk_box_pack_start (GTK_BOX (box), edit, FALSE, TRUE, 0);

    GtkWidget *add = gtk_button_new_from_icon_name ("list-add-symbolic", GTK_ICON_SIZE_BUTTON);
    g_object_set_data (G_OBJECT (add), "list", GINT_TO_POINTER (id));
    g_signal_connect (G_OBJECT (add), "clicked", G_CALLBACK (week_day_add_cb), entry);
    /*gtk_grid_attach(GTK_GRID(grid), add, 3, i, 1, 1); */
    gtk_box_pack_start (GTK_BOX (box), add, FALSE, TRUE, 0);
    gtk_widget_set_sensitive (add, used < 12);
  }

  gtk_widget_show_all (grid);

  return grid;
}

static void max_program(GSimpleAction *action,
                        GVariant      *parameter,
                        gpointer       user_data)
{
  gint address = g_variant_get_int32 (parameter);
  struct cube *cube = cube_get_active ();
  struct cube_device *device = cube_get_device_by_address (cube, address);
  GtkWidget *dialog;
  GtkWidget *grid;
  GtkWidget *content_area;
  GtkWidget *stack_switcher;
  gint ret;
  gint i;

  dialog = g_object_new (GTK_TYPE_DIALOG, "use-header-bar", TRUE, NULL);

  gtk_window_set_modal (GTK_WINDOW (dialog), TRUE);
  gtk_window_set_transient_for (GTK_WINDOW (dialog), GTK_WINDOW (max_main_window));
  gtk_dialog_add_button (GTK_DIALOG (dialog), _("Cancel"), GTK_RESPONSE_CANCEL);
  gtk_window_set_title (GTK_WINDOW (dialog), _("Week program"));
  GtkWidget *apply = gtk_dialog_add_button (GTK_DIALOG (dialog), _("Set"), GTK_RESPONSE_OK);
  gtk_style_context_add_class (gtk_widget_get_style_context (apply), GTK_STYLE_CLASS_SUGGESTED_ACTION);

  content_area = gtk_dialog_get_content_area (GTK_DIALOG (dialog));

  grid = gtk_grid_new ();
  gtk_grid_set_row_spacing (GTK_GRID (grid), 5);
  gtk_grid_set_column_spacing (GTK_GRID (grid), 5);
  gtk_widget_set_margin_start (grid, 5);
  gtk_widget_set_margin_end (grid, 5);
  gtk_widget_set_margin_top (grid, 5);
  gtk_widget_set_margin_bottom (grid, 5);

  stack_switcher = gtk_stack_switcher_new ();
  gtk_widget_set_hexpand (stack_switcher, FALSE);
  gtk_grid_attach (GTK_GRID (grid), stack_switcher, 0, 0, 1, 1);
  stack = gtk_stack_new ();
  gtk_grid_attach (GTK_GRID (grid), stack, 0, 1, 1, 1);
  gtk_stack_switcher_set_stack (GTK_STACK_SWITCHER (stack_switcher), GTK_STACK (stack));

  for (i = 0; i < 7; i++) {
    week_days[i] = g_list_copy (device->config->days[i]);
  }

  gtk_container_add (GTK_CONTAINER (content_area), grid);

  rebuild_week_ui ();

  gtk_widget_show_all (content_area);
  ret = gtk_dialog_run (GTK_DIALOG (dialog));

  if (ret != GTK_RESPONSE_OK) {
    gtk_widget_destroy (dialog);
    cube_device_request_config (cube, device);
    return;
  }

  gtk_widget_destroy (dialog);

  for (i = 0; i < 7; i++) {
    device->config->days[i] = g_list_copy (week_days[i]);
    cube_send_device_day_program (cube, device, i, week_days[i]);
  }
}

static void max_offset(GSimpleAction *action,
                       GVariant      *parameter,
                       gpointer       user_data)
{
  gint address = g_variant_get_int32 (parameter);
  struct cube *cube = cube_get_active ();
  struct cube_device *device = cube_get_device_by_address (cube, address);
  struct cube_room *room = cube_get_room_by_id (cube, device->room_id);
  GtkWidget *dialog;
  GtkWidget *grid;
  GtkWidget *content_area;
  GList *list;
  GList *buttons = NULL;
  gint ret;
  gint i = 0;

  dialog = g_object_new (GTK_TYPE_DIALOG, "use-header-bar", TRUE, NULL);

  gtk_window_set_modal (GTK_WINDOW (dialog), TRUE);
  gtk_window_set_transient_for (GTK_WINDOW (dialog), GTK_WINDOW (max_main_window));
  gtk_dialog_add_button (GTK_DIALOG (dialog), _("Cancel"), GTK_RESPONSE_CANCEL);
  gtk_window_set_title (GTK_WINDOW (dialog), _("Temperature Offsets"));
  GtkWidget *apply = gtk_dialog_add_button (GTK_DIALOG (dialog), _("Set"), GTK_RESPONSE_OK);
  gtk_style_context_add_class (gtk_widget_get_style_context (apply), GTK_STYLE_CLASS_SUGGESTED_ACTION);

  content_area = gtk_dialog_get_content_area (GTK_DIALOG (dialog));

  grid = gtk_grid_new ();
  gtk_widget_set_hexpand (grid, TRUE);

  gtk_grid_set_row_spacing (GTK_GRID (grid), 6);
  gtk_grid_set_column_spacing (GTK_GRID (grid), 12);
  gtk_widget_set_margin_start (grid, 18);
  gtk_widget_set_margin_end (grid, 18);
  gtk_widget_set_margin_top (grid, 18);
  gtk_widget_set_margin_bottom (grid, 18);

  gtk_container_add (GTK_CONTAINER (content_area), grid);

  for (list = room->devices; list != NULL; list = list->next) {
    struct cube_device *device = list->data;

    if (device->type == CUBE_DEVICE_TYPE_SHUTTER) {
      continue;
    }

    GtkWidget *label = ui_label_new (device->name->str);
    GtkWidget *spin_button = gtk_spin_button_new_with_range (-3.5, 3.5, 0.5);
    GtkWidget *degree = gtk_label_new ("°C");

    gtk_grid_attach (GTK_GRID (grid), label, 0, i, 1, 1);

    g_object_set_data (G_OBJECT (spin_button), "device", device);
    buttons = g_list_append (buttons, spin_button);
    gtk_spin_button_set_value (GTK_SPIN_BUTTON (spin_button), device->config->temp_offset);
    gtk_grid_attach (GTK_GRID (grid), spin_button, 1, i, 1, 1);
    gtk_grid_attach (GTK_GRID (grid), degree, 2, i++, 1, 1);
  }

  gtk_widget_show_all (content_area);
  ret = gtk_dialog_run (GTK_DIALOG (dialog));

  if (ret != GTK_RESPONSE_OK) {
    gtk_widget_destroy (dialog);
    return;
  }

  for (list = buttons, i = 0; list != NULL; list = list->next, i++) {
    GtkWidget *widget = list->data;
    struct cube_device *device = g_object_get_data (G_OBJECT (widget), "device");

    if (gtk_spin_button_get_value (GTK_SPIN_BUTTON (widget)) != device->config->temp_offset) {
      g_print ("Setting new offset value: %.1f\n", gtk_spin_button_get_value (GTK_SPIN_BUTTON (widget)));
      device->config->temp_offset = gtk_spin_button_get_value (GTK_SPIN_BUTTON (widget));
      cube_send_device_base_config (cube, device);
    }
  }

  gtk_widget_destroy (dialog);
}

static void max_display(GSimpleAction *action,
                        GVariant      *parameter,
                        gpointer       user_data)
{
  gint address = g_variant_get_int32 (parameter);
  struct cube *cube = cube_get_active ();
  struct cube_device *device = cube_get_device_by_address (cube, address);
  GtkWidget *dialog;
  GtkWidget *grid;
  GtkWidget *content_area;
  gint ret;

  dialog = g_object_new (GTK_TYPE_DIALOG, "use-header-bar", TRUE, NULL);

  gtk_window_set_modal (GTK_WINDOW (dialog), TRUE);
  gtk_window_set_transient_for (GTK_WINDOW (dialog), GTK_WINDOW (max_main_window));
  gtk_dialog_add_button (GTK_DIALOG (dialog), _("Cancel"), GTK_RESPONSE_CANCEL);
  gtk_window_set_title (GTK_WINDOW (dialog), _("Temperature Display"));
  GtkWidget *apply = gtk_dialog_add_button (GTK_DIALOG (dialog), _("Set"), GTK_RESPONSE_OK);
  gtk_style_context_add_class (gtk_widget_get_style_context (apply), GTK_STYLE_CLASS_SUGGESTED_ACTION);

  content_area = gtk_dialog_get_content_area (GTK_DIALOG (dialog));

  grid = gtk_grid_new ();
  gtk_widget_set_hexpand (grid, TRUE);

  gtk_grid_set_row_spacing (GTK_GRID (grid), 6);
  gtk_grid_set_column_spacing (GTK_GRID (grid), 12);
  gtk_widget_set_margin_start (grid, 18);
  gtk_widget_set_margin_end (grid, 18);
  gtk_widget_set_margin_top (grid, 18);
  gtk_widget_set_margin_bottom (grid, 18);

  gtk_container_add (GTK_CONTAINER (content_area), grid);

  GtkWidget *label = ui_label_new ("Display");
  gtk_grid_attach (GTK_GRID (grid), label, 0, 0, 1, 1);

  GtkWidget *combobox = gtk_combo_box_text_new ();
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combobox), _("Set temperature"));
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combobox), _("Room temperature"));
  gtk_combo_box_set_active (GTK_COMBO_BOX (combobox), device->valve != 0);
  gtk_grid_attach (GTK_GRID (grid), combobox, 1, 0, 1, 1);

  gtk_widget_show_all (content_area);
  ret = gtk_dialog_run (GTK_DIALOG (dialog));

  if (ret != GTK_RESPONSE_OK) {
    gtk_widget_destroy (dialog);
    return;
  }

  cube_set_wall_display (cube, device, gtk_combo_box_get_active (GTK_COMBO_BOX (combobox)));

  gtk_widget_destroy (dialog);
}

void max_add_room(GtkWidget *widget,
                  gpointer   user_data)
{
  struct cube *cube = user_data;
  GtkWidget *dialog;
  GtkWidget *content_area;
  GtkWidget *name;
  gint ret;

  dialog = gtk_dialog_new_with_buttons (_("Add new room"), GTK_WINDOW (max_main_window), GTK_DIALOG_MODAL, _("Create"), GTK_RESPONSE_OK, _("Cancel"), GTK_RESPONSE_CANCEL, NULL);
  content_area = gtk_dialog_get_content_area (GTK_DIALOG (dialog));

  name = gtk_entry_new ();
  gtk_entry_set_max_length (GTK_ENTRY (name), 31);
  gtk_container_add (GTK_CONTAINER (content_area), name);
  gtk_widget_show (name);
  ret = gtk_dialog_run (GTK_DIALOG (dialog));

  if (ret != GTK_RESPONSE_OK) {
    gtk_widget_destroy (dialog);
    return;
  }

  cube_add_room (cube, gtk_entry_get_text (GTK_ENTRY (name)));

  gtk_widget_destroy (dialog);
}

void house_auto_cb(GtkWidget *widget,
                   gpointer   user_data)
{
  struct cube *cube = user_data;
  GtkWidget *dialog;
  GtkWidget *content_area;
  gint ret;
  GList *list;
  GtkWidget *grid;
  gint i = 0;
  GList *buttons = NULL;

  /*dialog = gtk_dialog_new_with_buttons(_("Auto mode"), GTK_WINDOW(max_main_window), GTK_DIALOG_MODAL | GTK_DIALOG_USE_HEADER_BAR, _("Cancel"), GTK_RESPONSE_CANCEL, _("Apply"), GTK_RESPONSE_OK, NULL); */
  /*content_area = gtk_dialog_get_content_area(GTK_DIALOG(dialog)); */

  dialog = g_object_new (GTK_TYPE_DIALOG, "use-header-bar", TRUE, NULL);

  gtk_window_set_modal (GTK_WINDOW (dialog), TRUE);
  gtk_window_set_transient_for (GTK_WINDOW (dialog), GTK_WINDOW (max_main_window));
  gtk_dialog_add_button (GTK_DIALOG (dialog), _("Cancel"), GTK_RESPONSE_CANCEL);
  gtk_window_set_title (GTK_WINDOW (dialog), _("Auto mode"));
  GtkWidget *apply = gtk_dialog_add_button (GTK_DIALOG (dialog), _("Apply"), GTK_RESPONSE_OK);
  gtk_style_context_add_class (gtk_widget_get_style_context (apply), GTK_STYLE_CLASS_SUGGESTED_ACTION);

  content_area = gtk_dialog_get_content_area (GTK_DIALOG (dialog));

  grid = gtk_grid_new ();
  gtk_grid_set_row_spacing (GTK_GRID (grid), 6);
  gtk_grid_set_column_spacing (GTK_GRID (grid), 12);

  gtk_widget_set_margin_start (content_area, 18);
  gtk_widget_set_margin_end (content_area, 18);
  gtk_widget_set_margin_top (content_area, 18);
  gtk_widget_set_margin_bottom (content_area, 18);

  for (list = cube_get_active ()->rooms; list != NULL; list = list->next) {
    struct cube_room *room = list->data;
    GtkWidget *check_button = gtk_check_button_new_with_label (room->name->str);

    g_object_set_data (G_OBJECT (check_button), "room", room);
    buttons = g_list_append (buttons, check_button);
    gtk_grid_attach (GTK_GRID (grid), check_button, 0, i++, 1, 1);
  }

  gtk_container_add (GTK_CONTAINER (content_area), grid);
  gtk_widget_show_all (content_area);

  ret = gtk_dialog_run (GTK_DIALOG (dialog));

  if (ret != GTK_RESPONSE_OK) {
    gtk_widget_destroy (dialog);
    return;
  }

  for (list = buttons, i = 0; list != NULL; list = list->next, i++) {
    GtkWidget *widget = list->data;

    if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (widget))) {
      struct cube_room *room = g_object_get_data (G_OBJECT (widget), "room");
      struct cube_device *device = cube_get_device_by_address (cube, room->rf_address);

      if (!device) {
        device = cube_room_get_device (room, CUBE_DEVICE_TYPE_WALL);
      }

      if (device) {
        cube_set_device_temperature (cube, device, CUBE_MODE_AUTO, device->config->comfort_temp, 0, 0);
      }
    }
  }

  g_list_free (buttons);

  gtk_widget_destroy (dialog);
}

void house_comfort_cb(GtkWidget *widget,
                      gpointer   user_data)
{
  struct cube *cube = user_data;
  GtkWidget *dialog;
  GtkWidget *content_area;
  GtkWidget *calendar;
  GtkWidget *grid;
  GtkWidget *label;
  GtkWidget *combo;
  gint ret;
  GList *list;
  gint i = 0;
  GList *buttons = NULL;
  guint year;
  guint month;
  guint day;
  guint hour;

  /*dialog = gtk_dialog_new_with_buttons(_("Comfort mode"), GTK_WINDOW(max_main_window), GTK_DIALOG_MODAL | GTK_DIALOG_USE_HEADER_BAR, _("Cancel"), GTK_RESPONSE_CANCEL, _("Apply"), GTK_RESPONSE_OK, NULL); */
  /*content_area = gtk_dialog_get_content_area(GTK_DIALOG(dialog)); */
  dialog = g_object_new (GTK_TYPE_DIALOG, "use-header-bar", TRUE, NULL);

  gtk_window_set_modal (GTK_WINDOW (dialog), TRUE);
  gtk_window_set_transient_for (GTK_WINDOW (dialog), GTK_WINDOW (max_main_window));
  gtk_dialog_add_button (GTK_DIALOG (dialog), _("Cancel"), GTK_RESPONSE_CANCEL);
  gtk_window_set_title (GTK_WINDOW (dialog), _("Comfort mode"));
  GtkWidget *apply = gtk_dialog_add_button (GTK_DIALOG (dialog), _("Apply"), GTK_RESPONSE_OK);
  gtk_style_context_add_class (gtk_widget_get_style_context (apply), GTK_STYLE_CLASS_SUGGESTED_ACTION);

  content_area = gtk_dialog_get_content_area (GTK_DIALOG (dialog));

  grid = gtk_grid_new ();
  gtk_grid_set_row_spacing (GTK_GRID (grid), 6);
  gtk_grid_set_column_spacing (GTK_GRID (grid), 12);

  gtk_widget_set_margin_start (content_area, 18);
  gtk_widget_set_margin_end (content_area, 18);
  gtk_widget_set_margin_top (content_area, 18);
  gtk_widget_set_margin_bottom (content_area, 18);

  for (list = cube_get_active ()->rooms; list != NULL; list = list->next) {
    struct cube_room *room = list->data;
    GtkWidget *check_button = gtk_check_button_new_with_label (room->name->str);

    g_object_set_data (G_OBJECT (check_button), "room", room);
    buttons = g_list_append (buttons, check_button);
    gtk_grid_attach (GTK_GRID (grid), check_button, 0, i++, 2, 1);
  }

  calendar = gtk_calendar_new ();
  gtk_widget_set_hexpand (calendar, TRUE);
  gtk_grid_attach (GTK_GRID (grid), calendar, 0, i++, 2, 1);

  label = ui_label_new (_("Time:"));
  gtk_grid_attach (GTK_GRID (grid), label, 0, i, 1, 1);

  combo = gtk_combo_box_text_new ();
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combo), "00:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combo), "01:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combo), "02:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combo), "03:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combo), "04:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combo), "05:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combo), "06:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combo), "07:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combo), "08:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combo), "09:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combo), "10:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combo), "11:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combo), "12:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combo), "13:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combo), "14:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combo), "15:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combo), "16:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combo), "17:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combo), "18:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combo), "19:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combo), "20:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combo), "21:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combo), "22:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combo), "23:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combo), "24:00");
  gtk_combo_box_set_active (GTK_COMBO_BOX (combo), 12);
  gtk_widget_set_hexpand (combo, TRUE);
  gtk_grid_attach (GTK_GRID (grid), combo, 1, i++, 1, 1);

  gtk_container_add (GTK_CONTAINER (content_area), grid);
  gtk_widget_show_all (content_area);
  ret = gtk_dialog_run (GTK_DIALOG (dialog));

  if (ret != GTK_RESPONSE_OK) {
    gtk_widget_destroy (dialog);
    return;
  }

  gtk_calendar_get_date (GTK_CALENDAR (calendar), &year, &month, &day);
  hour = gtk_combo_box_get_active (GTK_COMBO_BOX (combo));

  month++;
  g_print ("year: %d\n", year);
  g_print ("month: %d\n", month);
  g_print ("day: %d\n", day);
  g_print ("hour: %d\n", hour);

  for (list = buttons, i = 0; list != NULL; list = list->next, i++) {
    GtkWidget *widget = list->data;

    if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (widget))) {
      struct cube_room *room = g_object_get_data (G_OBJECT (widget), "room");
      struct cube_device *device = cube_get_device_by_address (cube, room->rf_address);

      if (!device) {
        device = cube_room_get_device (room, CUBE_DEVICE_TYPE_WALL);
      }

      if (device) {
        g_print ("Setting vacation mode for room '%s'\n", room->name->str);
        /*cube_set_device_temperature(cube, device, CUBE_MODE_VACATION, device->config->comfort_temp, 0x9d0b); */
        cube_set_device_temperature (cube, device, CUBE_MODE_VACATION, device->config->comfort_temp, cube_compute_date (day, month, year), cube_compute_time (hour, 0));
      }
    }
  }

  gtk_widget_destroy (dialog);
}

void house_eco_cb(GtkWidget *widget,
                  gpointer   user_data)
{
  struct cube *cube = user_data;
  GtkWidget *dialog;
  GtkWidget *content_area;
  GtkWidget *calendar;
  GtkWidget *grid;
  GtkWidget *label;
  GtkWidget *combo;
  gint ret;
  GList *list;
  gint i = 0;
  GList *buttons = NULL;
  guint year;
  guint month;
  guint day;
  guint hour;

  /*dialog = gtk_dialog_new_with_buttons(_("Eco mode"), GTK_WINDOW(max_main_window), GTK_DIALOG_MODAL | GTK_DIALOG_USE_HEADER_BAR, _("Cancel"), GTK_RESPONSE_CANCEL, _("Apply"), GTK_RESPONSE_OK, NULL); */
  /*content_area = gtk_dialog_get_content_area(GTK_DIALOG(dialog)); */

  dialog = g_object_new (GTK_TYPE_DIALOG, "use-header-bar", TRUE, NULL);

  gtk_window_set_modal (GTK_WINDOW (dialog), TRUE);
  gtk_window_set_transient_for (GTK_WINDOW (dialog), GTK_WINDOW (max_main_window));
  gtk_dialog_add_button (GTK_DIALOG (dialog), _("Cancel"), GTK_RESPONSE_CANCEL);
  gtk_window_set_title (GTK_WINDOW (dialog), _("Eco mode"));
  GtkWidget *apply = gtk_dialog_add_button (GTK_DIALOG (dialog), _("Apply"), GTK_RESPONSE_OK);
  gtk_style_context_add_class (gtk_widget_get_style_context (apply), GTK_STYLE_CLASS_SUGGESTED_ACTION);

  content_area = gtk_dialog_get_content_area (GTK_DIALOG (dialog));

  grid = gtk_grid_new ();
  gtk_grid_set_row_spacing (GTK_GRID (grid), 6);
  gtk_grid_set_column_spacing (GTK_GRID (grid), 12);

  gtk_widget_set_margin_start (content_area, 18);
  gtk_widget_set_margin_end (content_area, 18);
  gtk_widget_set_margin_top (content_area, 18);
  gtk_widget_set_margin_bottom (content_area, 18);

  for (list = cube_get_active ()->rooms; list != NULL; list = list->next) {
    struct cube_room *room = list->data;
    GtkWidget *check_button = gtk_check_button_new_with_label (room->name->str);

    g_object_set_data (G_OBJECT (check_button), "room", room);
    buttons = g_list_append (buttons, check_button);
    gtk_grid_attach (GTK_GRID (grid), check_button, 0, i++, 2, 1);
  }

  calendar = gtk_calendar_new ();
  gtk_widget_set_hexpand (calendar, TRUE);
  gtk_grid_attach (GTK_GRID (grid), calendar, 0, i++, 2, 1);

  label = ui_label_new (_("Time:"));
  gtk_grid_attach (GTK_GRID (grid), label, 0, i, 1, 1);

  combo = gtk_combo_box_text_new ();
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combo), "00:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combo), "01:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combo), "02:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combo), "03:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combo), "04:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combo), "05:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combo), "06:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combo), "07:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combo), "08:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combo), "09:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combo), "10:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combo), "11:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combo), "12:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combo), "13:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combo), "14:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combo), "15:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combo), "16:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combo), "17:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combo), "18:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combo), "19:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combo), "20:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combo), "21:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combo), "22:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combo), "23:00");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combo), "24:00");
  gtk_combo_box_set_active (GTK_COMBO_BOX (combo), 12);
  gtk_widget_set_hexpand (combo, TRUE);
  gtk_grid_attach (GTK_GRID (grid), combo, 1, i++, 1, 1);

  gtk_container_add (GTK_CONTAINER (content_area), grid);
  gtk_widget_show_all (content_area);
  ret = gtk_dialog_run (GTK_DIALOG (dialog));

  if (ret != GTK_RESPONSE_OK) {
    gtk_widget_destroy (dialog);
    return;
  }

  gtk_calendar_get_date (GTK_CALENDAR (calendar), &year, &month, &day);
  hour = gtk_combo_box_get_active (GTK_COMBO_BOX (combo));

  month++;
  g_print ("year: %d\n", year);
  g_print ("month: %d\n", month);
  g_print ("day: %d\n", day);
  g_print ("hour: %d\n", hour);

  for (list = buttons, i = 0; list != NULL; list = list->next, i++) {
    GtkWidget *widget = list->data;

    if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (widget))) {
      struct cube_room *room = g_object_get_data (G_OBJECT (widget), "room");
      struct cube_device *device = cube_get_device_by_address (cube, room->rf_address);

      if (!device) {
        device = cube_room_get_device (room, CUBE_DEVICE_TYPE_WALL);
      }

      if (device) {
        g_print ("Setting eco mode for room '%s'\n", room->name->str);
        /*cube_set_device_temperature(cube, device, CUBE_MODE_VACATION, device->config->comfort_temp, 0x9d0b); */
        cube_set_device_temperature (cube, device, CUBE_MODE_VACATION, device->config->eco_temp, cube_compute_date (day, month, year), cube_compute_time (hour, 0));
      }
    }
  }

  gtk_widget_destroy (dialog);
}

static gulong h = 0;

void add_window_destroy_cb(GtkWidget *widget,
                           gpointer   user_data)
{
  struct cube *cube = user_data;

  cube_stop_device_scan (cube);

  if (h > 0) {
    g_signal_handler_disconnect (cube_object, h);
    h = 0;
  }
}

void assistant_apply(GtkAssistant *assistant,
                     gpointer      user_data)
{
  GtkWidget *device_entry = g_object_get_data (G_OBJECT (assistant), "device-name");
  GSList *radios = g_object_get_data (G_OBJECT (assistant), "room-list");
  GSList *list;
  const gchar *device_name = NULL;
  const gchar *room_name = NULL;
  struct cube *cube = user_data;
  struct cube_device *device = g_object_get_data (G_OBJECT (assistant), "device");

  device_name = gtk_entry_get_text (GTK_ENTRY (device_entry));
  g_print ("Device name: '%s'\n", device_name);

  for (list = radios; list != NULL; list = list->next) {
    GtkRadioButton *button = list->data;

    if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (button))) {
      room_name = gtk_button_get_label (GTK_BUTTON (button));
      g_print (" -> '%s'\n", room_name);

      if (!strcmp (room_name, _("New room:"))) {
        GtkWidget *entry = g_object_get_data (G_OBJECT (assistant), "entry");

        room_name = gtk_entry_get_text (GTK_ENTRY (entry));
        g_print (" -> '%s'\n", room_name);
      }
    }
  }

  if (device && device_name && room_name) {
    struct cube_room *room = cube_get_room_by_name (cube, room_name);

    if (!room) {
      room = cube_create_room (cube, room_name);
    }

    g_assert (room != NULL);

    device->name = g_string_new (device_name);

    cube_room_add_device (room, device);

    max_rebuild_ui (NULL, cube, NULL);
  }
}

void assistant_cancel(GtkWidget *assistant,
                      gpointer   user_data)
{
  struct cube *cube = user_data;

  cube_stop_device_scan (cube);

  if (h > 0) {
    g_signal_handler_disconnect (cube_object, h);
    h = 0;
  }

  gtk_widget_destroy (assistant);
}

void assistant_close(GtkAssistant *assistant,
                     gpointer      user_data)
{
  gtk_widget_destroy (GTK_WIDGET (assistant));
}

void assistant_prepare(GtkAssistant *assistant,
                       GtkWidget    *page,
                       gpointer      user_data)
{
  gint page_num = gtk_assistant_get_current_page (assistant);
  struct cube *cube = user_data;

  switch (page_num) {
    case 1: {
      GtkWidget *device_entry = g_object_get_data (G_OBJECT (assistant), "device-name");
      struct cube_device *device = g_object_get_data (G_OBJECT (assistant), "device");
      gchar *name = cube_propose_device_name (cube, device);

      cube_stop_device_scan (cube);
      if (h > 0) {
        g_signal_handler_disconnect (cube_object, h);
        h = 0;
      }

      gtk_entry_set_text (GTK_ENTRY (device_entry), name);
      g_free (name);
      break;
    }
    default:
      break;
  }
}

gboolean max_update_device_box(gpointer user_data)
{
  struct cube_device *device = user_data;
  GtkWidget *new_device;
  gchar *tmp;

  tmp = g_strdup_printf ("<b>%s</b>\n<small>(%s)</small>", cube_device_string (device->type), device->serial_number->str);

  new_device = gtk_label_new ("");
  gtk_label_set_markup (GTK_LABEL (new_device), tmp);
  gtk_widget_show (new_device);

  g_object_set_data (G_OBJECT (new_device), "device", device);

  gtk_list_box_prepend (GTK_LIST_BOX (max_new_device_box), new_device);

  return G_SOURCE_REMOVE;
}

static void max_new_device_detected(CubeObject         *obj,
                                    struct cube_device *device,
                                    gpointer            user_data)
{
  g_idle_add (max_update_device_box, device);
}

gpointer max_scan_devices(gpointer user_data)
{
  struct cube *cube = user_data;

  h = g_signal_connect (G_OBJECT (cube_object), "new-device", G_CALLBACK (max_new_device_detected), NULL);

  cube_start_device_scan (cube);

  return NULL;
}

void new_device_row_selected_cb(GtkListBox    *box,
                                GtkListBoxRow *row,
                                gpointer       user_data)
{
  GtkWidget *assistant = user_data;
  GtkWidget *child;
  GtkWidget *page;
  struct cube_device *device;

  g_print ("1\n");
  child = gtk_bin_get_child (GTK_BIN (row));
  if (!child) {
    return;
  }

  g_print ("2\n");
  device = g_object_get_data (G_OBJECT (child), "device");
  if (!device) {
    return;
  }

  g_print ("3\n");
  g_object_set_data (G_OBJECT (assistant), "device", device);
  page = g_object_get_data (G_OBJECT (assistant), "scanning");
  gtk_assistant_set_page_complete (GTK_ASSISTANT (assistant), GTK_WIDGET (page), TRUE);
}

void max_add_device(GtkWidget *widget,
                    gpointer   user_data)
{
  struct cube *cube = user_data;
  GtkWidget *assistant;
  GList *list;
  struct cube_device *device;
  GtkWidget *grid;

  assistant = gtk_assistant_new ();
  gtk_window_set_default_size (GTK_WINDOW (assistant), 300, 300);
  gtk_window_set_position (GTK_WINDOW (assistant), GTK_WIN_POS_CENTER);

  g_signal_connect (G_OBJECT (assistant), "apply", G_CALLBACK (assistant_apply), cube);
  g_signal_connect (G_OBJECT (assistant), "cancel", G_CALLBACK (assistant_cancel), cube);
  g_signal_connect (G_OBJECT (assistant), "close", G_CALLBACK (assistant_close), cube);
  g_signal_connect (G_OBJECT (assistant), "prepare", G_CALLBACK (assistant_prepare), cube);

  grid = gtk_grid_new ();

  GtkWidget *frame = gtk_frame_new (NULL);
  gtk_widget_set_hexpand (frame, TRUE);
  gtk_widget_set_vexpand (frame, TRUE);
  gtk_grid_attach (GTK_GRID (grid), frame, 0, 1, 2, 1);

  max_new_device_box = gtk_list_box_new ();
  GtkWidget *scan = gtk_label_new (_("Scanning..."));
  gtk_widget_show (scan);
  gtk_list_box_set_placeholder (GTK_LIST_BOX (max_new_device_box), scan);
  g_object_set_data (G_OBJECT (assistant), "scanning", grid);
  g_signal_connect (max_new_device_box, "row-selected", G_CALLBACK (new_device_row_selected_cb), assistant);
  gtk_container_add (GTK_CONTAINER (frame), max_new_device_box);
  gtk_assistant_append_page (GTK_ASSISTANT (assistant), grid);
  gtk_assistant_set_page_title (GTK_ASSISTANT (assistant), grid, _("Select device"));
  gtk_assistant_set_page_type (GTK_ASSISTANT (assistant), grid, GTK_ASSISTANT_PAGE_INTRO);

  GtkWidget *label = gtk_entry_new ();
  gtk_entry_set_max_length (GTK_ENTRY (label), 31);
  g_object_set_data (G_OBJECT (label), "scan-done", label);
  g_object_set_data (G_OBJECT (assistant), "device-name", label);

  gtk_assistant_append_page (GTK_ASSISTANT (assistant), label);
  gtk_assistant_set_page_title (GTK_ASSISTANT (assistant), label, _("Set device name"));
  gtk_assistant_set_page_complete (GTK_ASSISTANT (assistant), label, TRUE);
  gtk_assistant_set_page_type (GTK_ASSISTANT (assistant), label, GTK_ASSISTANT_PAGE_CONTENT);

  GtkWidget *room_grid = gtk_grid_new ();
  GSList *radios = NULL;
  GtkWidget *radio;
  gint i = 0;

  for (list = cube->rooms; list != NULL; list = list->next) {
    struct cube_room *room = list->data;

    if (!i) {
      radio = gtk_radio_button_new_with_label (radios, room->name->str);
    } else {
      radio = gtk_radio_button_new_with_label_from_widget (GTK_RADIO_BUTTON (radio), room->name->str);
    }

    gtk_grid_attach (GTK_GRID (room_grid), radio, 0, i++, 1, 1);
  }

  if (!i) {
    radio = gtk_radio_button_new_with_label (radios, _("New room:"));
  } else {
    radio = gtk_radio_button_new_with_label_from_widget (GTK_RADIO_BUTTON (radio), _("New room:"));
  }
  radios = gtk_radio_button_get_group (GTK_RADIO_BUTTON (radio));

  g_object_set_data (G_OBJECT (assistant), "room-list", radios);
  gtk_grid_attach (GTK_GRID (room_grid), radio, 0, i++, 1, 1);

  GtkWidget *entry = gtk_entry_new ();
  gtk_entry_set_max_length (GTK_ENTRY (entry), 31);
  gtk_grid_attach (GTK_GRID (room_grid), entry, 0, i++, 1, 1);
  g_object_set_data (G_OBJECT (assistant), "entry", entry);

  gtk_assistant_append_page (GTK_ASSISTANT (assistant), room_grid);
  gtk_assistant_set_page_title (GTK_ASSISTANT (assistant), room_grid, _("Add device to room"));
  gtk_assistant_set_page_complete (GTK_ASSISTANT (assistant), room_grid, TRUE);
  gtk_assistant_set_page_type (GTK_ASSISTANT (assistant), room_grid, GTK_ASSISTANT_PAGE_CONFIRM);

  for (list = cube->devices; list != NULL; list = list->next) {
    device = list->data;
    struct cube_room *device_room = cube_get_room_by_id (cube, device->room_id);

    if (!device_room) {
      gchar *tmp = g_strdup_printf ("<b>%s</b>\n<small>(%s)</small>", cube_device_string (device->type), device->serial_number->str);
      GtkWidget *new_device = gtk_label_new ("");
      gtk_label_set_markup (GTK_LABEL (new_device), tmp);
      g_object_set_data (G_OBJECT (new_device), "device", device);
      gtk_widget_show (new_device);
      gtk_list_box_prepend (GTK_LIST_BOX (max_new_device_box), new_device);
    }
  }

  gtk_widget_show_all (assistant);

  g_thread_new ("scan devices", max_scan_devices, cube);
}

static void max_app_startup(GtkApplication *app,
                            gpointer        user_data)
{
  static const GActionEntry actions[] = {
    {"rename_room", max_rename_room, "i"},
    {"remove_room", max_remove_room, "i"},
    {"remove_device", max_remove_device, "i"},
    {"rename_device", max_rename_device, "i"},
    {"temperature", max_temperature, "i"},
    {"boost", max_boost, "i"},
    {"program", max_program, "i"},
    {"offset", max_offset, "i"},
    {"display", max_display, "i"},
    {"about", about_activated, NULL, NULL, NULL},
    {"internet", max_internet, "i"},
    {"account", max_account, "i"},
    {"quit", quit_activated, NULL, NULL, NULL},
  };

  g_action_map_add_action_entries (G_ACTION_MAP (app), actions, G_N_ELEMENTS (actions), app);
}

static void max_app_activate(GtkApplication *app,
                             gpointer        user_data)
{
  GList *cubes;
  struct cube *cube;
  gchar *tmp;
  GMenu *menu;

  cube_init ();

  cubes = cube_detect (option_state.ip);

  if (!g_list_length (cubes)) {
    GtkWidget *dialog = gtk_message_dialog_new (NULL, GTK_DIALOG_MODAL, GTK_MESSAGE_ERROR, GTK_BUTTONS_CLOSE, _("No cubes detected"));

    g_print ("No cubes detected\n");
    gtk_dialog_run (GTK_DIALOG (dialog));
    gtk_widget_destroy (dialog);
    return;
  }

  menu = g_menu_new ();
  g_menu_append (menu, _("About"), "app.about");
  g_menu_append (menu, _("Quit"), "app.quit");

  /* Set cube to first one in list */
  cube = cubes->data;

  cube_set_active (cube);

  /* Create max_main_window */
  max_main_window = gtk_application_window_new (app);
  gtk_window_set_position (GTK_WINDOW (max_main_window), GTK_WIN_POS_CENTER);
  gtk_window_set_default_size (GTK_WINDOW (max_main_window), 640, 420);
  g_signal_connect (max_main_window, "destroy", G_CALLBACK (max_quit), cube);

  /* Create header bar */
  GtkWidget *headerbar = gtk_header_bar_new ();
  gtk_header_bar_set_show_close_button (GTK_HEADER_BAR (headerbar), TRUE);
  gtk_header_bar_set_title (GTK_HEADER_BAR (headerbar), "Max Control");
  tmp = g_strdup_printf ("Cube: %s", cube->detect_header.serial->str);
  gtk_header_bar_set_subtitle (GTK_HEADER_BAR (headerbar), tmp);
  g_free (tmp);

  GtkWidget *menu_button = gtk_menu_button_new ();
  gtk_header_bar_pack_end (GTK_HEADER_BAR (headerbar), menu_button);
  gtk_menu_button_set_direction (GTK_MENU_BUTTON (menu_button), GTK_ARROW_NONE);
  gtk_menu_button_set_menu_model (GTK_MENU_BUTTON (menu_button), G_MENU_MODEL (menu));

  /* Add add button to header bar (left) */
  GtkWidget *add_button = gtk_button_new_from_icon_name ("list-add-symbolic", GTK_ICON_SIZE_BUTTON);
  g_signal_connect (add_button, "clicked", G_CALLBACK (max_add_device), cube);
  gtk_header_bar_pack_start (GTK_HEADER_BAR (headerbar), add_button);
  gtk_window_set_titlebar ((GtkWindow *)(max_main_window), headerbar);

  GtkWidget *box = gtk_box_new (GTK_ORIENTATION_VERTICAL, 5);
  gtk_widget_set_margin_start (box, 6);
  gtk_widget_set_margin_end (box, 6);

  gtk_container_add (GTK_CONTAINER (max_main_window), box);

  /* House frame */
  GtkWidget *grid = gtk_grid_new ();
  GtkWidget *frame_label;

  gtk_grid_set_row_spacing (GTK_GRID (grid), 6);
  gtk_grid_set_column_spacing (GTK_GRID (grid), 12);

  gtk_widget_set_margin_start (grid, 6);
  gtk_widget_set_margin_end (grid, 6);
  gtk_widget_set_margin_top (grid, 6);
  gtk_widget_set_margin_bottom (grid, 6);

  GtkWidget *frame = gtk_frame_new ("");
  gtk_container_add (GTK_CONTAINER (frame), grid);
  gtk_widget_set_vexpand (frame, FALSE);
  gtk_widget_set_valign (frame, GTK_ALIGN_START);

  frame_label = gtk_frame_get_label_widget (GTK_FRAME (frame));
  tmp = g_strdup_printf (_("<b>House</b>"));
  gtk_label_set_markup (GTK_LABEL (frame_label), tmp);
  g_free (tmp);
  gtk_box_pack_start (GTK_BOX (box), frame, FALSE, FALSE, 5);

  GtkWidget *label = gtk_label_new (_("Mode:"));
  gtk_grid_attach (GTK_GRID (grid), label, 0, 0, 1, 1);

  GtkWidget *auto_button = gtk_button_new_with_label (_("AUTO"));
  g_signal_connect (auto_button, "clicked", G_CALLBACK (house_auto_cb), cube);
  gtk_widget_set_hexpand (auto_button, TRUE);
  gtk_grid_attach (GTK_GRID (grid), auto_button, 1, 0, 1, 1);

  GtkWidget *manu_button = gtk_button_new_with_label (_("COMFORT"));
  g_signal_connect (manu_button, "clicked", G_CALLBACK (house_comfort_cb), cube);
  gtk_widget_set_hexpand (manu_button, TRUE);
  gtk_grid_attach (GTK_GRID (grid), manu_button, 2, 0, 1, 1);

  GtkWidget *eco_button = gtk_button_new_with_label (_("ECO"));
  g_signal_connect (eco_button, "clicked", G_CALLBACK (house_eco_cb), cube);
  gtk_widget_set_hexpand (eco_button, TRUE);
  gtk_grid_attach (GTK_GRID (grid), eco_button, 3, 0, 1, 1);

  menu = g_menu_new ();

  GMenuItem *item;
  item = g_menu_item_new (_("Internet control"), "app.internet");
  g_menu_item_set_action_and_target (G_MENU_ITEM (item), "app.internet", "i", cube);
  g_menu_append_item (menu, item);

  item = g_menu_item_new (_("Account"), "app.account");
  g_menu_item_set_action_and_target (G_MENU_ITEM (item), "app.account", "i", cube);
  g_menu_append_item (menu, item);

  item = g_menu_item_new (_("Set timezone"), "app.set_timezone");
  g_menu_item_set_action_and_target (G_MENU_ITEM (item), "app.set_timezone", "i", cube);
  g_menu_append_item (menu, item);

  GtkWidget *settings = gtk_menu_button_new ();
  gtk_widget_set_hexpand (settings, FALSE);
  gtk_widget_set_halign (settings, GTK_ALIGN_END);
  gtk_menu_button_set_direction (GTK_MENU_BUTTON (settings), GTK_ARROW_NONE);
  gtk_menu_button_set_menu_model (GTK_MENU_BUTTON (settings), G_MENU_MODEL (menu));
  gtk_grid_attach (GTK_GRID (grid), settings, 3, 1, 1, 1);

  GtkWidget *scrolled_window = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_min_content_height (GTK_SCROLLED_WINDOW (scrolled_window), 140);
  gtk_box_pack_start (GTK_BOX (box), scrolled_window, TRUE, TRUE, 5);

  /* Add flow box to main max_main_window */
  max_room_flowbox = gtk_flow_box_new ();
  gtk_widget_set_hexpand (max_room_flowbox, TRUE);
  gtk_flow_box_set_selection_mode (GTK_FLOW_BOX (max_room_flowbox), GTK_SELECTION_NONE);
  gtk_flow_box_set_row_spacing (GTK_FLOW_BOX (max_room_flowbox), 5);
  gtk_flow_box_set_column_spacing (GTK_FLOW_BOX (max_room_flowbox), 5);
  gtk_flow_box_set_homogeneous (GTK_FLOW_BOX (max_room_flowbox), TRUE);
  gtk_container_add (GTK_CONTAINER (scrolled_window), max_room_flowbox);
  gtk_widget_show_all (max_main_window);

  g_signal_connect (G_OBJECT (cube_object), "modified", G_CALLBACK (max_rebuild_ui), NULL);
  g_signal_connect (G_OBJECT (cube_object), "status", G_CALLBACK (max_update_ui), NULL);

  /* Start max cube handling */
  max_start (cube);
}

const GOptionEntry all_options[] = {
  {"ip", 0, 0, G_OPTION_ARG_STRING, &option_state.ip, "Cube IP address", NULL},
  {NULL}
};

GOptionContext *max_app_options_get_context(void)
{
  GOptionContext *context;

  context = g_option_context_new ("-");
  g_option_context_add_main_entries (context, all_options, GETTEXT_PACKAGE);
  g_option_context_set_translation_domain (context, GETTEXT_PACKAGE);
  g_option_context_add_group (context, gtk_get_option_group (FALSE));

  return context;
}

static gint max_app_command_line_cb(GtkApplication          *app,
                                    GApplicationCommandLine *command_line,
                                    gpointer                 data)
{
  GOptionContext *context;
  int argc;
  char **argv;

  argv = g_application_command_line_get_arguments (command_line, &argc);

  memset (&option_state, 0, sizeof (option_state));

  context = max_app_options_get_context ();
  g_option_context_set_help_enabled (context, TRUE);
  if (g_option_context_parse (context, &argc, &argv, NULL) == FALSE) {
    g_option_context_free (context);
    g_strfreev (argv);
    return 1;
  }

  g_option_context_free (context);

  max_app_activate (app, NULL);

  g_strfreev (argv);

  return 0;
}

void max_app_shutdown(GApplication *app,
                      gpointer      user_data)
{
  g_mutex_lock (&max_ui_mutex);
}

/**
 * \brief main entry
 * \param argc argument count
 * \param argv argument vector
 * \return error code
 */
int main(int    argc,
         char **argv)
{
  GtkApplication *app;
  int status;

  g_set_prgname (PACKAGE_NAME);
  g_set_application_name (PACKAGE_NAME);

  /* Set local bindings */
  bindtextdomain (GETTEXT_PACKAGE, MAX_LOCALEDIR);
  bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
  textdomain (GETTEXT_PACKAGE);

  memset (&option_state, 0, sizeof (option_state));

  app = gtk_application_new ("org.tabos.maxcontrol", G_APPLICATION_HANDLES_COMMAND_LINE);
  g_signal_connect (app, "startup", G_CALLBACK (max_app_startup), NULL);
  g_signal_connect (app, "activate", G_CALLBACK (max_app_activate), NULL);
  g_signal_connect (app, "command-line", G_CALLBACK (max_app_command_line_cb), app);
  g_signal_connect (app, "shutdown", G_CALLBACK (max_app_shutdown), app);

  status = g_application_run (G_APPLICATION (app), argc, argv);

  return status;
}
